FROM golang:1.14 as builder

# Set Environment Variables
ENV HOME /
ENV CGO_ENABLED 1
ENV GOOS linux

WORKDIR /build
COPY go.mod go.sum ./
RUN go mod download
COPY . ./
COPY .git/ ./.git/

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt install -y nodejs
RUN npm install -g yarn

# Build app
RUN make unittest
RUN make build

# we don't use alpine here as we need a full glibc for sqlite to run
FROM debian:latest

WORKDIR /root

# Copy the pre-built binary file from the previous stage
COPY --from=builder /build/launchpad-backend .

EXPOSE 8888

CMD [ "./launchpad-backend" ]