# env
VERSION := $(shell git describe --tags)
BUILD := $(shell git rev-parse --short HEAD)
DATE := $(shell TZ=utc date +%FT%TZ)

PROJECTNAME := launchpad-backend
GOBASE := $(shell pwd)
TESTPACKAGES := $(shell go list ./... | grep -v '/model\|/static\|/backend$$')

# use linker flags to provide version/build settings
LDFLAGS=-ldflags "-X=gitlab.com/projectlaunchpad/backend/utils.Version=$(VERSION) -X=gitlab.com/projectlaunchpad/backend/utils.Build=$(BUILD)  -X=gitlab.com/projectlaunchpad/backend/utils.BuildDate=$(DATE)"
MAKEFLAGS += --silent

help:
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

start: clean build	## Start backend service
	@-$(GOBASE)/$(PROJECTNAME)

generate-backend:	## Generate static backend assets
	@go run -tags=dev static/assets_generate.go

build: build-linux

build-linux: generate-backend	## Build backend service for linux
	@echo -n "building $(PROJECTNAME) Linux binary.."
	@go build $(LDFLAGS) -o $(PROJECTNAME)
	@echo "done."

build-windows: generate-backend		## Build backend service for windows
	@echo -n "building $(PROJECTNAME) Windows binary.."
	@GOOS=windows GOARCH=amd64 go build $(LDFLAGS) -o $(PROJECTNAME).exe
	@echo "done."

build-mac-x86: generate-backend		## Build backend service for windows
	@echo -n "building $(PROJECTNAME) MacOS (x86) binary.."
	@GOOS=darwin GOARCH=amd64 go build $(LDFLAGS) -o $(PROJECTNAME).macos.x86
	@echo "done."

run: 
	@go run $(LDFLAGS) main.go

gofmt: ## Run formatting on go files
	@go fmt ./...

test: generate-backend	## Run unit tests
	@echo "running tests.."
	TESTMODE=true
	@go test $(LDFLAGS) $(TESTPACKAGES)

unittest: generate-backend	## Run unit tests
	@echo "running unit tests for" $(VERSION) $(BUILD)
	@go test $(LDFLAGS) $(TESTPACKAGES)

clean:	## Clean build cache
	@echo -n "cleaning build cache.."
	@-rm $(GOBASE)/$(PROJECTNAME) > /dev/null 2>&1
	@-rm $(GOBASE)/static/generated_assets.go > /dev/null 2>&1
	@go clean
	@echo "done."