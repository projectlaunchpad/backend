# Rush for the Web Backend

This is the Rush for the Web backend. It manages game state and game flow when playing using the mobile app. All state is currently hold in memory, restarting the service causes all running games to be terminated.

## Testing

The implementation has unittest that can be run using

```
make unittest
```

The output will contain the result of the test run.

## Building and Running

To build the backend, issue the command 

```
make build
```

This builds binaries for Linux, MacOS and Windows. The binary is self-contained and contains all resources. To run it on a server, just put the binary file on the machine and run it.

You can also use the supplied `Dockerfile` for a container-based deployment. 

## Runtime Configuration

The following environment variables can be used to configure the runtime behaviour:

* `launchpad_externalhttpurl` contains the external URL for the service (defaults to `http://localhost:8888`).
* `httpcertfile` contains the relative path to the certificate used for HTTPS protocol binding, defaults to "" (HTTPS disabled).
* `httpkeyfile` contains the relative path to the key used for HTTPS protocol binding, defaults to "" (HTTPS disabled).
* `httpurl` contains the local bind URL and port, defaults to `:8888`.

On startup, the service logs the recognized env parameters.
