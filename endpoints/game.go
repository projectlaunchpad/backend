/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package endpoints provides http endpoints.
package endpoints

import (
	"encoding/json"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/rules"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

// NewGameEndpoint returns a new endpoint instance.
func NewGameEndpoint(config *viper.Viper, storage *storage.Storage) (*GameEndpoint, error) {
	endpoint := &GameEndpoint{}
	endpoint.config = config
	endpoint.storage = storage
	return endpoint, nil
}

// GameEndpoint represents the endpoint.
type GameEndpoint struct {
	config  *viper.Viper
	storage *storage.Storage
}

// checks for presence of game and player in context, adds errors if not.
func checkGetParams(c *gin.Context) (model.Game, model.Player, bool) {
	gameI, exists := c.Get("game")
	if !exists {
		log.Printf("no game in context")
		c.String(500, "no game in context")
	}
	playerI, exists := c.Get("player")
	if !exists {
		log.Printf("no player in context")
		c.String(500, "no player in context")
	}
	game, ok := gameI.(model.Game)
	if !ok {
		log.Printf("game in context not of correct type")
		c.String(500, "game in context not of correct type")
		return model.Game{}, model.Player{}, false
	}
	player, ok := playerI.(model.Player)
	if !ok {
		log.Printf("player in context not of correct type")
		c.String(500, "player in context not of correct type")
		return model.Game{}, model.Player{}, false
	}
	return game, player, true
}

// GetGame implements the GetGame endpoint feature.
func (endpoint *GameEndpoint) GetGameState(c *gin.Context) {
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	if !utils.ContainsString(game.PlayerIDs, player.ID) {
		c.String(403, "player is not in the requested game")
		return
	}
	// we remove all actions except the last one
	if len(game.ActionIDs) > 0 {
		game.ActionIDs = game.ActionIDs[len(game.ActionIDs)-1:]
	}
	response, err := json.Marshal(game)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetPlayer implements the GetPlayer endpoint feature.
func (endpoint *GameEndpoint) GetPlayer(c *gin.Context) {
	game, _, ok := checkGetParams(c)
	if !ok {
		return
	}
	playerID := c.Param("playerid")
	if playerID == "" {
		c.String(500, "playerid can not be empty")
		return
	}
	if !utils.ContainsString(game.PlayerIDs, playerID) {
		c.String(403, "requested playerid does not match game playerids")
		return
	}
	player, exists := endpoint.storage.GetPlayerByID(playerID)
	if !exists {
		c.String(500, "player not found")
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetProject implements the GetProject endpoint feature.
func (endpoint *GameEndpoint) GetProject(c *gin.Context) {
	projectID := c.Param("projectid")
	if projectID == "" {
		c.String(500, "projectid can not be empty")
		return
	}
	project, exists := endpoint.storage.GetProjectByID(projectID)
	if !exists {
		c.String(500, "project not found")
		return
	}
	response, err := json.Marshal(project)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetAction implements the GetAction endpoint feature.
func (endpoint *GameEndpoint) GetAction(c *gin.Context) {
	actionID := c.Param("actionid")
	if actionID == "" {
		c.String(500, "actionid can not be empty")
		return
	}
	action, exists := endpoint.storage.GetActionByID(actionID)
	if !exists {
		c.String(500, "action not found")
		return
	}
	response, err := json.Marshal(action)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetStack implements the GetStack endpoint feature.
func (endpoint *GameEndpoint) GetStack(c *gin.Context) {
	game, _, ok := checkGetParams(c)
	if !ok {
		return
	}
	stackID := c.Param("stackid")
	if stackID == "" {
		c.String(500, "stackid can not be empty")
		return
	}
	if !utils.ContainsString([]string{
		game.EventStackID,
		game.EventDiscardStackID,
		game.PersonaStackID,
		game.PersonaDiscardStackID,
		game.ResourceStackID,
		game.ResourceDiscardStackID,
		game.ProjectStackID,
		game.ProjectDiscardStackID,
		game.PoolStackID,
	}, stackID) {
		c.String(403, "requested stackid does not match game stacks")
		return
	}
	stack, exists := endpoint.storage.GetStackByID(stackID)
	if !exists {
		c.String(500, "stack not found")
		return
	}
	// FIXME: remove cards based on permissions
	response, err := json.Marshal(stack)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetHand implements the GetHand endpoint feature.
func (endpoint *GameEndpoint) GetHand(c *gin.Context) {
	_, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	handID := c.Param("handid")
	if handID == "" {
		c.String(500, "handid can not be empty")
		return
	}
	if handID != player.HandID {
		c.String(403, "handid does not match player handid")
		return
	}
	hand, exists := endpoint.storage.GetHandByID(handID)
	if !exists {
		c.String(500, "hand not found")
		return
	}
	response, err := json.Marshal(hand)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

// GetAllCards implements the GetAllCards endpoint feature.
func (endpoint *GameEndpoint) GetAllCards(c *gin.Context) {
	allCards, err := endpoint.storage.GetAllCards()
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(allCards)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) CreateGame(c *gin.Context) {
	var err error
	playerName := c.Param("playername")
	if playerName == "" {
		c.String(500, "player name can not be empty")
		return
	}
	player, err := rules.CreateGame(*endpoint.storage, playerName)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) StartGame(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	game, err = rules.StartGame(*endpoint.storage, game.ID, player.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	log.Printf("started game %s", game.ID+" state is now "+game.Status)
	response, err := json.Marshal(game)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) JoinGame(c *gin.Context) {
	var err error
	playerName := c.Param("playername")
	if playerName == "" {
		c.String(500, "player name can not be empty")
		return
	}
	invitecode := c.Param("invitecode")
	if invitecode == "" {
		c.String(500, "invite code can not be empty")
		return
	}
	game, exists := endpoint.storage.GetGameByInviteCode(invitecode)
	if !exists {
		c.String(500, "invite code invalid")
		return
	}
	player, err := rules.JoinGame(*endpoint.storage, game.ID, playerName)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) DrawResourceCardFromDrawDeck(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardID, err := rules.DrawResourceCard(*endpoint.storage, game.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	player, err = rules.AddResouceCardToPlayerHand(*endpoint.storage, game.ID, player.ID, cardID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	card, exists := endpoint.storage.GetCardByID(cardID)
	if !exists {
		c.String(500, "card not found")
		return
	}
	response, err := json.Marshal(card)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) GetResourceCardFromPool(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardID, err := rules.GetResourceCardFromPool(*endpoint.storage, game.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	player, err = rules.AddResouceCardToPlayerHand(*endpoint.storage, game.ID, player.ID, cardID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	card, exists := endpoint.storage.GetCardByID(cardID)
	if !exists {
		c.String(500, "card not found")
		return
	}
	response, err := json.Marshal(card)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) DiscardResourceCardToPool(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardID := c.Param("resourcecardid")
	if cardID == "" {
		c.String(500, "card id can not be empty")
		return
	}
	game, err = rules.MoveResourceCardFromHandToPool(*endpoint.storage, game.ID, player.ID, cardID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(game)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) AquireProjectCard(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardIDs, err := rules.AquireProjectCard(*endpoint.storage, game.ID, player.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(cardIDs)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) AcceptProjectCard(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardID := c.Param("projectcardid")
	if cardID == "" {
		c.String(500, "card id can not be empty")
		return
	}
	player, err = rules.AcceptProjectCard(*endpoint.storage, game.ID, player.ID, cardID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) MoveResourceCardFromHandToProject(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	projectID := c.Param("projectid")
	if projectID == "" {
		c.String(500, "project id can not be empty")
		return
	}
	resourceCardID := c.Param("resourcecardid")
	if resourceCardID == "" {
		c.String(500, "resource card id can not be empty")
		return
	}
	player, err = rules.MoveResourceCardFromHandToProject(*endpoint.storage, game.ID, player.ID, projectID, resourceCardID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) SellProject(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	projectID := c.Param("projectid")
	if projectID == "" {
		c.String(500, "project id can not be empty")
		return
	}
	player, err = rules.SellProject(*endpoint.storage, game.ID, player.ID, projectID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) TurnComplete(c *gin.Context) {
	var err error
	game, player, ok := checkGetParams(c)
	if !ok {
		return
	}
	game, err = rules.EndTurn(*endpoint.storage, game.ID, player.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(game)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) EndGame(c *gin.Context) {
	var err error
	game, _, ok := checkGetParams(c)
	if !ok {
		return
	}
	gameResult, err := rules.EndGame(*endpoint.storage, game.ID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(gameResult)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) GiveResourceCardToPlayer(c *gin.Context) {
	var err error
	game, sourcePlayer, ok := checkGetParams(c)
	if !ok {
		return
	}
	cardID := c.Param("resourcecardid")
	if cardID == "" {
		c.String(500, "card id can not be empty")
		return
	}
	targetPlayerID := c.Param("playerid")
	if targetPlayerID == "" {
		c.String(500, "target player id id can not be empty")
		return
	}
	player, err := rules.MoveResourceCardToPlayer(*endpoint.storage, game.ID, sourcePlayer.ID, cardID, targetPlayerID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}

func (endpoint *GameEndpoint) GiveCreditsToPlayer(c *gin.Context) {
	var err error
	game, sourcePlayer, ok := checkGetParams(c)
	if !ok {
		return
	}
	creditsValStr := c.Param("creditsvalue")
	if creditsValStr == "" {
		c.String(500, "card id can not be empty")
		return
	}
	creditsVal, err := strconv.Atoi(creditsValStr)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	targetPlayerID := c.Param("playerid")
	if targetPlayerID == "" {
		c.String(500, "target player id id can not be empty")
		return
	}
	player, err := rules.MoveCreditsToPlayer(*endpoint.storage, game.ID, sourcePlayer.ID, creditsVal, targetPlayerID)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
		return
	}
	response, err := json.Marshal(player)
	if err != nil {
		log.Println(err)
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(response)
	}
}
