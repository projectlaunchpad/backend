/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package endpoints provides http endpoints.
package endpoints

import (
	"encoding/json"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

// NewStatusEndpoint returns a new endpoint instance.
func NewStatusEndpoint(config *viper.Viper, storage *storage.Storage) (*StatusEndpoint, error) {
	endpoint := &StatusEndpoint{}
	endpoint.config = config
	endpoint.storage = storage
	return endpoint, nil
}

// StatusEndpoint represents the endpoint.
type StatusEndpoint struct {
	config  *viper.Viper
	storage *storage.Storage
}

// GetStatus implements the GetStatus enpoint feature.
func (endpoint *StatusEndpoint) GetStatus(c *gin.Context) {
	responseMessage := &model.Status{
		ID:        utils.CreateUUID(),
		Status:    utils.SystemStatus(),
		Version:   utils.Version,
		Build:     utils.Build,
		BuildDate: utils.BuildDate,
	}
	status, err := json.Marshal(responseMessage)
	if err != nil {
		c.String(500, err.Error())
	} else {
		c.Status(200)
		c.Header("Content-Type", "application/json")
		c.Writer.Write(status)
	}
}
