/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/projectlaunchpad/backend/service"
	"gitlab.com/projectlaunchpad/backend/utils"
)

func newConfig() (*viper.Viper, error) {

	c := viper.New()
	c.SetEnvPrefix("launchpad")

	c.SetDefault("httpurl", ":8888")
	pflag.String("httpurl", ":8888", "http service bind url pattern")

	c.SetDefault("externalhttpurl", "http://localhost:8888")
	pflag.String("externalhttpurl", "http://localhost:8888", "external url of server")

	c.SetDefault("httpcertfile", "")
	pflag.String("httpcertfile", "", "https certification file path")

	c.SetDefault("httpkeyfile", "")
	pflag.String("httpkeyfile", "", "https key file path")

	c.AutomaticEnv()

	pflag.Parse()
	c.BindPFlags(pflag.CommandLine)

	return c, nil
}

func main() {
	config, err := newConfig()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Launchpad Backend, version %s %s built on %s\n\n", utils.Version, utils.Build, utils.BuildDate)
	configStr, _ := json.MarshalIndent(config.AllSettings(), "", "  ")
	fmt.Println("Using configuration:")
	fmt.Println(string(configStr))

	app, err := service.NewApplication(config)
	if err != nil {
		log.Fatal(err)
	}
	defer app.Close()
	// starting the application
	app.Start()
}
