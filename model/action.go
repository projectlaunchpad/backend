/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const ActionGameStart = "game_start"
const ActionGameEnd = "game_end"
const ActionNextTurn = "next_turn"
const ActionGameEnded = "next_turn"
// FIXME: add more here

const SQLCreateActions = `
CREATE TABLE IF NOT EXISTS actions (
	ID TEXT PRIMARY KEY,
	timestamp INTEGER NOT NULL,
	type TEXT NOT NULL,
	sourcheid TEXT NOT NULL,
	targetid TEXT NOT NULL,
	objectids TEXT NOT NULL,
	name TEXT NOT NULL
)
`

// Action describes an action.
type Action struct {
	ID        string   `json:"ID"`
	Timestamp int64    `json:"timestamp"`
	Type      string   `json:"type"`
	SourceID  string   `json:"sourceid"`
	TargetID  string   `json:"targetid"`
	ObjectIDs []string `json:"objectids"`
	Name      string   `json:"name"`
}
