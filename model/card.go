/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateCards = `
CREATE TABLE IF NOT EXISTS cards (
	ID TEXT PRIMARY KEY,
	type TEXT NOT NULL,
	name TEXT NOT NULL,
	description TEXT NOT NULL,
	illustrationid TEXT NOT NULL,
	iconid TEXT NOT NULL,
	effectid TEXT NOT NULL,
	effectargs TEXT NOT NULL,
	colors TEXT NOT NULL,
	cost INTEGER NOT NULL,
	payout TEXT NOT NULL)
`

const CardTypeResource = "resource"
const CardTypeProject = "project"
const CardTypeEvent = "event"
const CardTypePersona = "persona"

// Card describes a card.
type Card struct {
	ID             string      `json:"ID"`
	Type           string      `json:"type"`
	Name           []string    `json:"name"`
	Description    []string    `json:"description"`
	IllustrationID string      `json:"illustrationid"`
	IconID         string      `json:"iconid"`
	EffectID       string      `json:"effectid"`
	EffectArgs     []string    `json:"effectargs"`
	Colors         []string    `json:"colors"`
	Cost           int         `json:"cost"`
	Payout         map[int]int `json:"payout"`
}
