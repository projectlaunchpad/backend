/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateGames = `
CREATE TABLE IF NOT EXISTS games (
	ID TEXT PRIMARY KEY,
	status TEXT NOT NULL,
	invitecode TEXT NOT NULL,
	playerids TEXT NOT NULL,
	stackids TEXT NOT NULL,
	iconid TEXT NOT NULL,
	poolstackid TEXT NOT NULL,
	resourcestackid TEXT NOT NULL,
	resourcediscardstackid TEXT NOT NULL,
	projectstackid TEXT NOT NULL,
	projectdiscardstackid TEXT NOT NULL,
	eventstackid TEXT NOT NULL,
	eventdiscardstackid TEXT NOT NULL,
	personastackid TEXT NOT NULL,
	personadiscardstackid TEXT NOT NULL,
	currenteventcardid TEXT NOT NULL,
	actionids TEXT NOT NULL,
	currentplayerid TEXT NOT NULL,
	firstplayerid TEXT NOT NULL)
`

const GameStateNew string = "new"
const GameStateRunning string = "running"
const GameStateEnded string = "ended"

// Game describes a game.
type Game struct {
	ID                     string   `json:"ID"`
	Status                 string   `json:"status"`
	InviteCode             string   `json:"invitecode"`
	PlayerIDs              []string `json:"playerids"`
	PoolStackID            string   `json:"poolstackid"`
	ResourceStackID        string   `json:"resourcestackid"`
	ResourceDiscardStackID string   `json:"resourcediscardstackid"`
	ProjectStackID         string   `json:"projectstackid"`
	ProjectDiscardStackID  string   `json:"projectdiscardstackid"`
	EventStackID           string   `json:"eventstackid"`
	EventDiscardStackID    string   `json:"eventdiscardstackid"`
	PersonaStackID         string   `json:"personastackid"`
	PersonaDiscardStackID  string   `json:"personadiscardstackid"`
	CurrentEventCardID     string   `json:"currenteventcardid"`
	ActionIDs              []string `json:"actionids"`
	CurrentPlayerID        string   `json:"currentplayerid"`
	FirstPlayerID          string   `json:"firstplayerid"`
}
