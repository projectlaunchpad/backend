/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateGameResults = `
CREATE TABLE IF NOT EXISTS gameresults (
	ID TEXT PRIMARY KEY,
	playerids TEXT NOT NULL,
	winnerplayerid TEXT NOT NULL,
	credits TEXT NOT NULL)
`

// GameResult describes a game result.
type GameResult struct {
	ID                     string   `json:"ID"`
	PlayerIDs              []string `json:"playerids"`
	WinnerPlayerID         string   `json:"winnerplayerid"`
	Credits                []int    `json:"credits"`
}
