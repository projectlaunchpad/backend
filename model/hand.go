/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateHands = `
CREATE TABLE IF NOT EXISTS hands (
	ID TEXT PRIMARY KEY,
	cardids TEXT NOT NULL)
`

// Hand describes a hand.
type Hand struct {
	ID      string   `json:"ID"`
	Size    int      `json:"size"`
	CardIDs []string `json:"cardids"`
}
