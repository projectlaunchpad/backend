/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreatePlayers = `
CREATE TABLE IF NOT EXISTS players (
	ID TEXT PRIMARY KEY,
	gameid TEXT NOT NULL,
	sessionToken TEXT NOT NULL,
	name TEXT NOT NULL,
	personacardid TEXT NOT NULL,
	handid TEXT NOT NULL,
	isfirstplayer BOOLEAN NOT NULL,
	credits INTEGER NOT NULL,
	projectcardids TEXT NOT NULL,
	cardchoicebufferids TEXT NOT NULL)
`

// Player describes a player.
type Player struct {
	ID                  string   `json:"ID"`
	GameID              string   `json:"gameid"`
	SessionToken        string   `json:"sessionToken"`
	Name                string   `json:"name"`
	PersonaCardID       string   `json:"personacardid"`
	HandID              string   `json:"handid"`
	IsFirstPlayer       bool     `json:"isfirstplayer"`
	Credits             int      `json:"credits"`
	ProjectIDs          []string `json:"projectids"`
	CardChoiceBufferIDs []string `json:"cardchoicebufferids"`
}
