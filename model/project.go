/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateProjects = `
CREATE TABLE IF NOT EXISTS projects (
	ID TEXT PRIMARY KEY,
	cardid TEXT NOT NULL,
	playerid TEXT NOT NULL,
	resourcecardids TEXT NOT NULL,
	colors TEXT NOT NULL,
	payout BOOLEAN NOT NULL,
	playeffectid INTEGER NOT NULL,
	effectargs TEXT NOT NULL)
`

// Project describes a project.
type Project struct {
	ID              string      `json:"ID"`
	CardID          string      `json:"cardid"`
	PlayerID        string      `json:"playerid"`
	ResourceCardIDs []string    `json:"resourcecardids"`
	Colors          []string    `json:"colors"`
	Payout          map[int]int `json:"payout"`
	EffectID        string      `json:"playeffectid"`
	EffectArgs      []string    `json:"effectargs"`
}
