/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

const SQLCreateStacks = `
CREATE TABLE IF NOT EXISTS projects (
	ID TEXT PRIMARY KEY,
	type TEXT NOT NULL,
	cardids TEXT NOT NULL)
`

const StackTypeResources string = "resources"
const StackTypeProjects string = "projects"
const StackTypeEvents string = "events"
const StackTypePersonas string = "personas"
const StackTypeResourceDiscard string = "resourcediscard"
const StackTypeProjectDiscard string = "projectdiscard"
const StackTypeEventDiscard string = "eventdiscard"
const StackTypePersonaDiscard string = "personadiscard"
const StackTypePool string = "pool"

// Stack describes a stack.
type Stack struct {
	ID      string   `json:"ID"`
	Type    string   `json:"type"`
	Size    int      `json:"size"`
	CardIDs []string `json:"cardids"`
}
