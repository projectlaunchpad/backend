/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package model provides game models.
package model

// Status describes a status.
type Status struct {
	ID        string `json:"ID"`
	Status    string `json:"status"`
	Version   string `json:"version"`
	Build     string `json:"build"`
	BuildDate string `json:"builddate"`
}
