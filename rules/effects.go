/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package rules provides game rules.
package rules

import (
	"fmt"
	"log"
	"strconv"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

const EffectSellBonus = "sell_bonus"
const EffectSellBonusColor = "sell_bonus_color"
const EffectCreditBonus = "credit_bonus"

const ActionHasProjectWithCards = "projectwithcards" // player gets bonus at start of turn with effect card active
const ActionPlayToPool = "topool" // player gets bonus when playing a card to the pool
const ActionPlayToProject = "toproject" // player gets bonus when playing a card to a project

const ScopeSoldProject = "sold_project"
const ScopePlayedCardToProject = "played_card_to_project"
const ScopePlayedToPool = "played_card_to_pool"
const ScopeBeginningOfTurn = "beginning_of_turn"

// applyEffect applies an effect to a game
func applyEffect(storage storage.Storage, scope string, effectID string, effectArgs []string, game model.Game, player model.Player, project model.Project) (model.Player, error) {
	if effectID == "" || game.ID == "" || player.ID == "" {
		return model.Player{}, fmt.Errorf("applyEffect called with invalid params for effectID %s", effectID)
	} 
	switch effectID {
		case EffectSellBonus: 
			if scope != ScopeSoldProject {
				log.Printf("effect EffectSellBonus not applied to scope %s", scope)
				return player, nil
			}
			if len(effectArgs) != 1 {
				return model.Player{}, fmt.Errorf("effectArgs for sell_bonus has not exactly one entry: %v", effectArgs)
			}
			value, err := strconv.Atoi(effectArgs[0])
			if err != nil {
				return model.Player{}, fmt.Errorf("effectArgs for sell_bonus has not a valid number: %v", effectArgs)
			}
			return applySellBonus(storage, player, project, value)
		case EffectSellBonusColor:
			if scope != ScopeSoldProject {
				log.Printf("effect EffectSellBonusColor not applied to scope %s", scope)
				return player, nil
			}
			if len(effectArgs) < 2 {
				return model.Player{}, fmt.Errorf("effectArgs for sell_bonus has not more than one entry: %v", effectArgs)
			}
			value, err := strconv.Atoi(effectArgs[0])
			if err != nil {
				return model.Player{}, fmt.Errorf("effectArgs for sell_bonus_color has not a valid number: %v", effectArgs)
			}
			return applySellBonusColor(storage, player, project, value, effectArgs[1:])
		case EffectCreditBonus:
			if len(effectArgs) != 2 {
				return model.Player{}, fmt.Errorf("effectArgs for credit_bonus has not exactly two entries: %v", effectArgs)
			}
			value, err := strconv.Atoi(effectArgs[0])
			if err != nil {
				return model.Player{}, fmt.Errorf("effectArgs for credit_bonus has not a valid number: %v", effectArgs)
			}
			if scope == ScopeSoldProject {
				log.Printf("effect EffectCreditBonus not applied to scope %s", scope)
				return player, nil
			}
			if scope == ScopePlayedCardToProject {
				return applyCreditBonus(storage, player, project, value, effectArgs[1], ActionPlayToProject)
			}
			if scope == ScopePlayedToPool {
				return applyCreditBonus(storage, player, project, value, effectArgs[1], ActionPlayToPool)
			}
			if scope == ScopeBeginningOfTurn {
				// run over projects, check every one if it has cards, if so, apply bonus (increase multiplier)
				projectsWithCards := 0
				for _, projectID := range player.ProjectIDs {
					project, exists := storage.GetProjectByID(projectID)
					if !exists {
						return model.Player{}, fmt.Errorf("project with id %s does not exist", projectID)
					}
					if len(project.ResourceCardIDs) > 0 {
						projectsWithCards++
					}
				}
				log.Printf("player has %d projects with cards relevant for effect, value %d", projectsWithCards, (value * projectsWithCards))
				return applyCreditBonus(storage, player, project, (value * projectsWithCards), effectArgs[1], ActionHasProjectWithCards)
			}
			return model.Player{}, fmt.Errorf("unknown scope %s for effect id %s", scope, effectID)
		default:
			log.Printf("ignoring unknown effect id %s", effectID)
			return player, nil
	}
}

// applyCreditBonus applies a credit bonus to a player, values are 'projectwithcards', 'topool', 'toproject'; applies instant credits if a condition is met
func applyCreditBonus(storage storage.Storage, player model.Player, project model.Project, value int, target string, performedAction string) (model.Player, error) {
	if target == "" || performedAction == "" {
		return model.Player{}, fmt.Errorf("applyCreditBonus called with invalid params for target %s and performedAction %s", target, performedAction)
	}
	if target == performedAction {
		log.Printf("applying instant credit bonus of %d for action %s", value, performedAction)
		player.Credits += value
		err := storage.PutPlayer(player)
		if err != nil {
			return model.Player{}, err
		}
		return player, nil
	}
	log.Printf("player not eligible for bonus, performed %s, target %s", performedAction, target)
	return player, nil
}

// applySellBonusColor applies a sell bonus to a player if project has cards with colors
func applySellBonusColor(storage storage.Storage, player model.Player, project model.Project, value int, colors []string) (model.Player, error) {
	if project.ID == "" {
		return model.Player{}, fmt.Errorf("applySellBonusColor called with invalid params for projectID %s", project.ID)
	}
	hasSpecifiedColors := false
	for _, cardID := range project.ResourceCardIDs {
		card, exists := storage.GetCardByID(cardID)
		if !exists {
			return model.Player{}, fmt.Errorf("project %s has a card with id %s that does not exist", project.ID, cardID)
		}
		if utils.ContainsString(colors, card.Colors[0]) {
			hasSpecifiedColors = true
			break
		}
	}
	if !hasSpecifiedColors {
		log.Printf("no cards in project %s have specified colors %v", project.ID, colors)
		return player, nil
	}
	log.Printf("cards in project %s match specified colors %v, applying sell bonus of %d", project.ID, colors, value)
	player.Credits += value
	err := storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	return player, nil
}

// applySellBonus applies a sell bonus to a player
func applySellBonus(storage storage.Storage, player model.Player, project model.Project, value int) (model.Player, error) {
	log.Printf("applying sell bonus of %d", value)
	player.Credits += value
	err := storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	return player, nil
}

