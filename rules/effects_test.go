/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package rules_test

import (
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/rules"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

func setEvent(storageInst *storage.Storage, game model.Game, effectID string, effectArgs []string) (model.Card, error) {
	eventCard := model.Card{
		ID:             "test-effects-eventcard-" + utils.CreateInviteCode(),
		Type:           model.CardTypeEvent,
		Name:           []string{},
		Description:    []string{},
		IllustrationID: "",
		IconID:         "",
		EffectID:       effectID,
		EffectArgs:     effectArgs,
		Colors:         []string{},
		Cost:           0,
		Payout:         map[int]int{},
	}
	err := storageInst.PutCard(eventCard)
	if err != nil {
		return model.Card{}, err
	}
	game.CurrentEventCardID = eventCard.ID
	err = storageInst.PutGame(game)
	if err != nil {
		return model.Card{}, err
	}
	return eventCard, nil
}

func TestEffects(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	// starting from a clean database
	os.Remove(storage.DatabaseFile)
	storageInst, err := storage.NewStorage(config)
	require.NoError(t, err)
	require.NotNil(t, storageInst)
	defer func() {
		storageInst.Close()
		os.Remove(storage.DatabaseFile)
	}()

	// prep test data
	player0Name := "player0name"
	player0, err := rules.CreateGame(*storageInst, player0Name)
	require.NoError(t, err)
	game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
	require.NoError(t, err)

	personaCard0 := model.Card{
		ID:             "test-effects-personacard-0",
		Type:           model.CardTypePersona,
		Name:           []string{},
		Description:    []string{},
		IllustrationID: "",
		IconID:         "",
		EffectID:       "sell_bonus_color",
		EffectArgs:     []string{"5", "blue"},
		Colors:         []string{},
		Cost:           0,
		Payout:         map[int]int{},
	}
	err = storageInst.PutCard(personaCard0)
	require.NoError(t, err)

	// force persona on player
	player0.PersonaCardID = personaCard0.ID
	err = storageInst.PutPlayer(player0)
	require.NoError(t, err)

	// create custom resource card 0
	resourceCard0 := model.Card{
		ID:             "test-effects-resourcecard-0",
		Type:           model.CardTypeResource,
		Name:           []string{},
		Description:    []string{},
		IllustrationID: "",
		IconID:         "",
		EffectID:       "sell_bonus",
		EffectArgs:     []string{"5"},
		Colors:         []string{"blue"},
		Cost:           1,
		Payout:         map[int]int{},
	}
	err = storageInst.PutCard(resourceCard0)
	require.NoError(t, err)

	// create custom resource card 1
	resourceCard1 := model.Card{
		ID:             "test-effects-resourcecard-1",
		Type:           model.CardTypeResource,
		Name:           []string{},
		Description:    []string{},
		IllustrationID: "",
		IconID:         "",
		EffectID:       "",
		EffectArgs:     []string{},
		Colors:         []string{"blue"},
		Cost:           1,
		Payout:         map[int]int{},
	}
	err = storageInst.PutCard(resourceCard1)
	require.NoError(t, err)

	// create custom resource card 2
	resourceCard2 := model.Card{
		ID:             "test-effects-resourcecard-2",
		Type:           model.CardTypeResource,
		Name:           []string{},
		Description:    []string{},
		IllustrationID: "",
		IconID:         "",
		EffectID:       "",
		EffectArgs:     []string{},
		Colors:         []string{"green"},
		Cost:           1,
		Payout:         map[int]int{},
	}
	err = storageInst.PutCard(resourceCard2)
	require.NoError(t, err)

	// create custom project
	project0 := model.Project{
		ID:              "test-effects-project-0",
		CardID:          "",
		PlayerID:        "",
		ResourceCardIDs: []string{},
		Colors:          []string{"blue", "green"},
		Payout: map[int]int{
			1: 1,
			2: 2,
			3: 3,
		},
		EffectID:   "",
		EffectArgs: []string{},
	}
	err = storageInst.PutProject(project0)
	require.NoError(t, err)

	project1 := model.Project{
		ID:              "test-effects-project-1",
		CardID:          "",
		PlayerID:        "",
		ResourceCardIDs: []string{},
		Colors:          []string{"blue", "green"},
		Payout: map[int]int{
			1: 1,
			2: 2,
			3: 3,
		},
		EffectID:   "",
		EffectArgs: []string{},
	}
	err = storageInst.PutProject(project1)
	require.NoError(t, err)

	// add cards and project to player
	hand, exists := storageInst.GetHandByID(player0.HandID)
	require.True(t, exists)
	hand.CardIDs = append(hand.CardIDs, resourceCard0.ID)
	hand.CardIDs = append(hand.CardIDs, resourceCard1.ID)
	hand.CardIDs = append(hand.CardIDs, resourceCard2.ID)
	err = storageInst.PutHand(hand)
	require.NoError(t, err)
	player0.ProjectIDs = []string{project0.ID, project1.ID}
	player0.Credits = 3
	err = storageInst.PutPlayer(player0)
	require.NoError(t, err)

	t.Run("event card with credit_bonus, projectwithcards and player cycling turns", func(t *testing.T) {
		// set event card
		eventCard, err := setEvent(storageInst, game, "credit_bonus", []string{"5", "projectwithcards"})
		require.NoError(t, err)
		game, exists = storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, eventCard.ID, game.CurrentEventCardID)

		// add card to project
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project1.ID, resourceCard2.ID)
		require.NoError(t, err)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 2, dbPlayer.Credits)

		// cycle player turns
		game, err := rules.EndTurn(*storageInst, game.ID, player0.ID)
		require.NoError(t, err)
		// there is only one player, so it cycles right back
		require.Equal(t, player0.ID, game.CurrentPlayerID)
		
		// beginning of turn effect adds 5 credits for the one project with cards
		dbPlayer, exists = storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 7, dbPlayer.Credits)
	})

	t.Run("event card with credit_bonus, toproject and player playing to project", func(t *testing.T) {
		// set event card
		eventCard, err := setEvent(storageInst, game, "credit_bonus", []string{"5", "toproject"})
		require.NoError(t, err)
		game, exists = storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, eventCard.ID, game.CurrentEventCardID)

		// add cards to project
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project0.ID, resourceCard0.ID)
		require.NoError(t, err)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 11, dbPlayer.Credits)

		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project0.ID, resourceCard1.ID)
		require.NoError(t, err)
		dbPlayer, exists = storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 15, dbPlayer.Credits)
	})

	t.Run("event card with credit_bonus, topool and player playing to pool", func(t *testing.T) {
		// set event card
		eventCard, err := setEvent(storageInst, game, "credit_bonus", []string{"5", "topool"})
		require.NoError(t, err)
		game, exists = storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, eventCard.ID, game.CurrentEventCardID)

		// play to pool
		_, err = rules.MoveResourceCardFromHandToPool(*storageInst, game.ID, player0.ID, resourceCard2.ID)
		require.NoError(t, err)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 20, dbPlayer.Credits)
	})

	t.Run("resource card with sell_bonus in sold project and event with sell_bonus", func(t *testing.T) {
		// set event card
		eventCard, err := setEvent(storageInst, game, "sell_bonus", []string{"5"})
		require.NoError(t, err)
		game, exists = storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, eventCard.ID, game.CurrentEventCardID)

		// sell project
		_, err = rules.SellProject(*storageInst, game.ID, player0.ID, project0.ID)
		require.NoError(t, err)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		// 20 base, 2 from selling, 2xResources with 5 bonus each, 5 from event with sell_bonus = 33
		require.Equal(t, 37, dbPlayer.Credits)
	})
}
