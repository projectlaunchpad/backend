/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package rules provides game rules.
package rules

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

const InitialCredits int = 3
const InitialHandSize int = 3
const InitialProjectSize int = 2
const MaxPlayersPerGame = 4

// randomElement returns a random element from the array
func randomElement(arr []string) string {
	return arr[rand.Intn(len(arr))]
}

// removeStringElement removes the element from the array
func removeStringElement(slice []string, s string) []string {
	for i, v := range slice {
		if v == s {
			return append(slice[:i], slice[i+1:]...)
		}
	}
	return slice
}

// applyResourcePlayEffect applies the effects that are attached to a resource card
func applyResourcePlayEffect(storage storage.Storage, gameID string, playerID string, projectID string, resourceCardID string, scope string) (model.Player, error) {
	var err error
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not exist", playerID)
	}
	resourceCard, exists := storage.GetCardByID(resourceCardID)
	if !exists {
		return model.Player{}, fmt.Errorf("card %s does not exist", resourceCardID)
	}
	// project may be empty
	project := model.Project{}
	if projectID != "" {
		project, exists = storage.GetProjectByID(projectID)
		if !exists {
			return model.Player{}, fmt.Errorf("project %s does not exist", projectID)
		}
	}
	// apply cost if played to project
	if scope == ScopePlayedCardToProject {
		if player.Credits < resourceCard.Cost {
			return model.Player{}, fmt.Errorf("player %s does not have enough credits to play this card", playerID)
		}
		player.Credits -= resourceCard.Cost
		err := storage.PutPlayer(player)
		if err != nil {
			return model.Player{}, err
		}
	}
	// apply effect of resource card
	if resourceCard.EffectID != "" {
		player, err = applyEffect(storage, scope, resourceCard.EffectID, resourceCard.EffectArgs, game, player, project)
		if err != nil {
			return model.Player{}, err
		}
	}
	// apply event effect, check if there is a event effect
	eventCard, exists := storage.GetCardByID(game.CurrentEventCardID)
	if !exists {
		return model.Player{}, fmt.Errorf("event card %s does not exist", game.CurrentEventCardID)
	}
	if eventCard.EffectID != "" {
		player, err = applyEffect(storage, scope, eventCard.EffectID, eventCard.EffectArgs, game, player, project)
		if err != nil {
			return model.Player{}, err
		}
	}
	return player, nil
}

// applyProjectPlayEffect applies the effects that are attached to a project card
func applyProjectSellEffect(storage storage.Storage, gameID string, playerID string, projectID string) (model.Player, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a pool", playerID)
	}
	project, exists := storage.GetProjectByID(projectID)
	if !exists {
		return model.Player{}, fmt.Errorf("project %s does not exist", projectID)
	}
	// apply sell credit gain
	for _, color := range project.Colors {
		numberOfCards := 0
		for _, cardID := range project.ResourceCardIDs {
			card, exists := storage.GetCardByID(cardID)
			if !exists {
				return model.Player{}, fmt.Errorf("card %s does not exist", cardID)
			}
			if len(card.Colors) != 1 {
				return model.Player{}, fmt.Errorf("card %s has not exactly one color", cardID)
			}
			if card.Colors[0] == color {
				numberOfCards++
			}
		}
		player.Credits += project.Payout[numberOfCards]
	}
	err := storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	// apply effect, check if there is a persona effect
	personaCard, exists := storage.GetCardByID(player.PersonaCardID)
	if !exists {
		return model.Player{}, fmt.Errorf("persona card %s does not exist", player.PersonaCardID)
	}
	if personaCard.EffectID != "" {
		player, err = applyEffect(storage, ScopeSoldProject, personaCard.EffectID, personaCard.EffectArgs, game, player, project)
		if err != nil {
			return model.Player{}, err
		}
	}
	// apply effect, check if there is a event effect
	eventCard, exists := storage.GetCardByID(game.CurrentEventCardID)
	if !exists {
		return model.Player{}, fmt.Errorf("event card %s does not exist", game.CurrentEventCardID)
	}
	if eventCard.EffectID != "" {
		player, err = applyEffect(storage, ScopeSoldProject, eventCard.EffectID, eventCard.EffectArgs, game, player, project)
		if err != nil {
			return model.Player{}, err
		}
	}
	// check every resource card on the project for sell_effect
	for _, cardID := range project.ResourceCardIDs {
		card, exists := storage.GetCardByID(cardID)
		if !exists {
			return model.Player{}, fmt.Errorf("card %s does not exist", cardID)
		}
		if card.EffectID != "" {
			player, err = applyEffect(storage, ScopeSoldProject, card.EffectID, card.EffectArgs, game, player, project)
			if err != nil {
				return model.Player{}, err
			}
		}
	}
	return player, nil
}

// drawCardFromStack returns a random card from the given stack, removing it from the stack
func drawRemoveCard(storage storage.Storage, stack model.Stack) (string, error) {
	if len(stack.CardIDs) == 0 {
		log.Printf("stack %s (stack type %s) has no cards", stack.ID, stack.Type)
		return "", fmt.Errorf("stack %s (stack type %s) has no cards", stack.ID, stack.Type)
	}
	cardID := randomElement(stack.CardIDs)
	stack.CardIDs = removeStringElement(stack.CardIDs, cardID)
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	log.Printf("drawed card %s from stack %s (stack type %s): %d cards left in stack", cardID, stack.ID, stack.Type, len(stack.CardIDs))
	return cardID, nil
}

func CreatePoolStack(storage storage.Storage, gameID string) (string, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return "", fmt.Errorf("game %s does not exist", gameID)
	}
	resourceStack, exists := storage.GetStackByID(game.ResourceStackID)
	if !exists {
		return "", fmt.Errorf("resource stack for this game does not exist, can not create pool stack")
	}
	cardID, err := drawRemoveCard(storage, resourceStack)
	if err != nil {
		return "", err
	}
	if cardID == "" {
		return "", fmt.Errorf("no cards left in resource stack")
	}
	poolStack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypePool,
		CardIDs: []string{cardID},
	}
	err = storage.PutStack(poolStack)
	if err != nil {
		return "", err
	}
	return poolStack.ID, nil
}

func CreateResourceStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeResources,
		CardIDs: []string{},
	}
	stack.CardIDs = make([]string, len(storage.ResourceCardIDs))
	copy(stack.CardIDs, storage.ResourceCardIDs)
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, err
}

func CreateResourceDiscardStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeResourceDiscard,
		CardIDs: []string{},
	}
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

func CreateProjectStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeProjects,
		CardIDs: []string{},
	}
	stack.CardIDs = make([]string, len(storage.ProjectCardIDs))
	copy(stack.CardIDs, storage.ProjectCardIDs)
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

func CreateProjectDiscardStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeProjectDiscard,
		CardIDs: []string{},
	}
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

func CreateEventStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeEvents,
		CardIDs: []string{},
	}
	stack.CardIDs = make([]string, len(storage.EventCardIDs))
	copy(stack.CardIDs, storage.EventCardIDs)
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, err
}

func CreateEventDiscardStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypeEventDiscard,
		CardIDs: []string{},
	}
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

func CreatePersonaStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypePersonas,
		CardIDs: []string{},
	}
	stack.CardIDs = make([]string, len(storage.PersonaCardIDs))
	copy(stack.CardIDs, storage.PersonaCardIDs)
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

func CreatePersonaDiscardStack(storage storage.Storage) (string, error) {
	stack := model.Stack{
		ID:      utils.CreateUUID(),
		Type:    model.StackTypePersonaDiscard,
		CardIDs: []string{},
	}
	err := storage.PutStack(stack)
	if err != nil {
		return "", err
	}
	return stack.ID, nil
}

// DrawCardByType returns a random card ID, removig it from the respective stack
func DrawCardByType(storage storage.Storage, gameID string, cardType string) (string, error) {
	// FIXME: implement stack cycling if empty
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return "", fmt.Errorf("game %s does not exist", gameID)
	}
	var stackID string
	switch cardType {
	case model.StackTypeResources:
		stackID = game.ResourceStackID
	case model.StackTypeProjects:
		stackID = game.ProjectStackID
	case model.StackTypeEvents:
		stackID = game.EventStackID
	case model.StackTypePersonas:
		stackID = game.PersonaStackID
	case model.StackTypeResourceDiscard:
		stackID = game.ResourceDiscardStackID
	case model.StackTypeProjectDiscard:
		stackID = game.ProjectDiscardStackID
	case model.StackTypeEventDiscard:
		stackID = game.EventDiscardStackID
	case model.StackTypePersonaDiscard:
		stackID = game.PersonaDiscardStackID
	default:
		return "", fmt.Errorf("unknown stack type %s", cardType)
	}
	stack, exists := storage.GetStackByID(stackID)
	if !exists {
		return "", fmt.Errorf("stack %s for game %s does not exist", stackID, gameID)
	}
	cardID, err := drawRemoveCard(storage, stack)
	if err != nil {
		return "", err
	}
	log.Printf("drew card of type %s for game %s: %s", cardType, gameID, cardID)
	return cardID, nil
}

// DrawPersonaCard returns a random card ID, removig it from the stack
func DrawPersonaCard(storage storage.Storage, gameID string) (string, error) {
	cardID, err := DrawCardByType(storage, gameID, model.StackTypePersonas)
	if err != nil {
		return "", err
	}
	return cardID, nil
}

// DrawResourceCard returns a random card ID, removig it from the stack
func DrawResourceCard(storage storage.Storage, gameID string) (string, error) {
	cardID, err := DrawCardByType(storage, gameID, model.StackTypeResources)
	if err != nil {
		return "", err
	}
	return cardID, nil
}

// DrawProjectCard returns a random card ID, removig it from the stack
func DrawProjectCard(storage storage.Storage, gameID string) (string, error) {
	cardID, err := DrawCardByType(storage, gameID, model.StackTypeProjects)
	if err != nil {
		return "", err
	}
	return cardID, nil
}

// DrawEventCard returns a random card ID, removig it from the stack
func DrawEventCard(storage storage.Storage, gameID string) (string, error) {
	cardID, err := DrawCardByType(storage, gameID, model.StackTypeEvents)
	if err != nil {
		return "", err
	}
	return cardID, nil
}

func CreateGame(storage storage.Storage, playerName string) (model.Player, error) {
	var err error
	hand := model.Hand{
		ID:      utils.CreateUUID(),
		CardIDs: []string{},
	}
	player := model.Player{
		ID:                  utils.CreateUUID(),
		SessionToken:        utils.CreateUUID(),
		Name:                playerName,
		PersonaCardID:       "",
		HandID:              hand.ID,
		IsFirstPlayer:       true,
		Credits:             InitialCredits,
		ProjectIDs:          []string{},
		CardChoiceBufferIDs: []string{},
	}
	resourceStack, err := CreateResourceStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	resourceDiscardStack, err := CreateResourceDiscardStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	projectStack, err := CreateProjectStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	projectDiscardStack, err := CreateProjectDiscardStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	eventStack, err := CreateEventStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	eventDiscardStack, err := CreateEventDiscardStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	personaStack, err := CreatePersonaStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	personaDiscardStack, err := CreatePersonaDiscardStack(storage)
	if err != nil {
		return model.Player{}, err
	}
	game := model.Game{
		ID:                     utils.CreateUUID(),
		Status:                 model.GameStateNew,
		InviteCode:             utils.CreateInviteCode(),
		PlayerIDs:              []string{player.ID},
		PoolStackID:            "",
		ResourceStackID:        resourceStack,
		ResourceDiscardStackID: resourceDiscardStack,
		ProjectStackID:         projectStack,
		ProjectDiscardStackID:  projectDiscardStack,
		EventStackID:           eventStack,
		EventDiscardStackID:    eventDiscardStack,
		PersonaStackID:         personaStack,
		PersonaDiscardStackID:  personaDiscardStack,
		CurrentEventCardID:     "",
		ActionIDs:              []string{},
		CurrentPlayerID:        "",
		FirstPlayerID:          player.ID,
	}
	err = storage.PutGame(game)
	if err != nil {
		return model.Player{}, err
	}
	poolStackID, err := CreatePoolStack(storage, game.ID)
	if err != nil {
		return model.Player{}, err
	}
	game.PoolStackID = poolStackID
	err = storage.PutGame(game)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("created new game %s", game.ID)
	player.GameID = game.ID
	player.PersonaCardID, err = DrawPersonaCard(storage, game.ID)
	if err != nil {
		return model.Player{}, err
	}
	player.ProjectIDs, err = GetInitialProjects(storage, game.ID, player.ID)
	if err != nil {
		return model.Player{}, err
	}
	hand.CardIDs, err = GetInitialResourceHand(storage, game.ID)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutHand(hand)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("created new hand %s for game %s", hand.ID, game.ID)
	err = storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("created new player %s for game %s", player.ID, game.ID)
	return player, nil
}

func GetInitialProjects(storage storage.Storage, gameID string, playerID string) ([]string, error) {
	cardIDs := []string{}
	projectIDs := []string{}
	for i := 0; i < InitialProjectSize; i++ {
		cardID, err := DrawProjectCard(storage, gameID)
		if err != nil {
			return nil, err
		}
		cardIDs = append(cardIDs, cardID)
	}
	// create the project entities, store them
	for _, projectCardID := range cardIDs {
		card, exists := storage.GetCardByID(projectCardID)
		if !exists {
			return nil, fmt.Errorf("project card %s not found", projectCardID)
		}
		project := model.Project{
			ID:              utils.CreateUUID(),
			CardID:          projectCardID,
			PlayerID:        playerID,
			ResourceCardIDs: []string{},
			Colors:          card.Colors,
			Payout:          card.Payout,
			EffectID:        card.EffectID,
			EffectArgs:      card.EffectArgs,
		}
		err := storage.PutProject(project)
		if err != nil {
			return nil, err
		}
		projectIDs = append(projectIDs, project.ID)
	}
	return projectIDs, nil
}

func GetInitialResourceHand(storage storage.Storage, gameID string) ([]string, error) {
	cardIDs := []string{}
	for i := 0; i < InitialHandSize; i++ {
		cardID, err := DrawResourceCard(storage, gameID)
		if err != nil {
			return nil, err
		}
		cardIDs = append(cardIDs, cardID)
	}
	return cardIDs, nil
}

func JoinGame(storage storage.Storage, gameID string, playerName string) (model.Player, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	if game.Status != model.GameStateNew {
		return model.Player{}, fmt.Errorf("game %s does is not new", gameID)
	}
	if len(game.PlayerIDs) >= MaxPlayersPerGame {
		return model.Player{}, fmt.Errorf("game %s is full", gameID)
	}
	resourceHand, err := GetInitialResourceHand(storage, game.ID)
	if err != nil {
		return model.Player{}, err
	}
	hand := model.Hand{
		ID:      utils.CreateUUID(),
		CardIDs: resourceHand,
	}
	err = storage.PutHand(hand)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("created new hand %s for game %s", hand.ID, game.ID)
	randomPersona, err := DrawPersonaCard(storage, game.ID)
	if err != nil {
		return model.Player{}, err
	}
	player := model.Player{
		ID:                  utils.CreateUUID(),
		GameID:              game.ID,
		SessionToken:        utils.CreateUUID(),
		Name:                playerName,
		PersonaCardID:       randomPersona,
		HandID:              hand.ID,
		IsFirstPlayer:       false,
		Credits:             InitialCredits,
		ProjectIDs:          []string{},
		CardChoiceBufferIDs: []string{},
	}
	player.ProjectIDs, err = GetInitialProjects(storage, game.ID, player.ID)
	if err != nil {
		return model.Player{}, err
	}
	game.PlayerIDs = append(game.PlayerIDs, player.ID)
	err = storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("created new player %s for game %s", player.ID, game.ID)
	err = storage.PutGame(game)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("updated game %s", game.ID)
	return player, nil
}

func StartGame(storage storage.Storage, gameID string, playerID string) (model.Game, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not exist", gameID)
	}
	game.Status = model.GameStateRunning
	game.FirstPlayerID = playerID
	game.CurrentPlayerID = playerID
	eventCard, err := DrawEventCard(storage, gameID)
	if err != nil {
		return model.Game{}, err
	}
	game.CurrentEventCardID = eventCard
	err = storage.PutGame(game)
	if err != nil {
		return model.Game{}, err
	}
	LogAction(storage, gameID, "Game started", model.ActionGameStart, playerID, "", nil);
	return game, nil
}

func AddResouceCardToPlayerHand(storage storage.Storage, gameID string, playerID string, cardID string) (model.Player, error) {
	_, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not exist", gameID)
	}
	playerHand, exists := storage.GetHandByID(player.HandID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a hand", playerID)
	}
	playerHand.CardIDs = append(playerHand.CardIDs, cardID)
	err := storage.PutHand(playerHand)
	if err != nil {
		return model.Player{}, err
	}
	return player, nil
}

func GetResourceCardFromPool(storage storage.Storage, gameID string) (string, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return "", fmt.Errorf("game %s does not exist", gameID)
	}
	poolStack, exists := storage.GetStackByID(game.PoolStackID)
	if !exists {
		return "", fmt.Errorf("game %s does not have a pool", gameID)
	}
	if len(poolStack.CardIDs) == 0 {
		log.Printf("pool stack %s has no cards", poolStack.ID)
		return "", fmt.Errorf("pool stack %s has no cards", poolStack.ID)
	}
	cardID := poolStack.CardIDs[0]
	poolStack.CardIDs = removeStringElement(poolStack.CardIDs, cardID)
	err := storage.PutStack(poolStack)
	if err != nil {
		return "", err
	}
	log.Printf("got card %s from pool stack %s: %d cards left in stack", cardID, poolStack.ID, len(poolStack.CardIDs))
	return cardID, nil
}

func AddResourceCardToPool(storage storage.Storage, gameID string, cardID string) (model.Game, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not exist", gameID)
	}
	poolStack, exists := storage.GetStackByID(game.PoolStackID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not have a pool", gameID)
	}
	poolStack.CardIDs = append([]string{cardID}, poolStack.CardIDs...)
	err := storage.PutStack(poolStack)
	if err != nil {
		return model.Game{}, err
	}
	log.Printf("added card %s to pool stack %s: %d cards in stack", cardID, poolStack.ID, len(poolStack.CardIDs))
	return game, nil
}

func MoveResourceCardFromHandToPool(storage storage.Storage, gameID string, playerID string, resourcecardID string) (model.Game, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not exist", gameID)
	}
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Game{}, fmt.Errorf("player %s does not exist", playerID)
	}
	playerHand, exists := storage.GetHandByID(player.HandID)
	if !exists {
		return model.Game{}, fmt.Errorf("player %s does not have a hand", playerID)
	}
	playerHand.CardIDs = removeStringElement(playerHand.CardIDs, resourcecardID)
	err := storage.PutHand(playerHand)
	if err != nil {
		return model.Game{}, err
	}
	poolStack, exists := storage.GetStackByID(game.PoolStackID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not have a pool", gameID)
	}
	poolStack.CardIDs = append([]string{resourcecardID}, poolStack.CardIDs...)
	err = storage.PutStack(poolStack)
	if err != nil {
		return model.Game{}, err
	}
	player, err = applyResourcePlayEffect(storage, gameID, playerID, "", resourcecardID, ScopePlayedToPool)
	if err != nil {
		return model.Game{}, err
	}
	log.Printf("moved card %s from player %s to pool stack %s: %d cards in stack", resourcecardID, playerID, poolStack.ID, len(poolStack.CardIDs))
	return game, nil
}

func AquireProjectCard(storage storage.Storage, gameID string, playerID string) ([]string, error) {
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return []string{}, fmt.Errorf("player %s does not have a pool", playerID)
	}
	pCard0, err := DrawProjectCard(storage, gameID)
	if err != nil {
		return []string{}, err
	}
	pCard1, err := DrawProjectCard(storage, gameID)
	if err != nil {
		return []string{}, err
	}
	player.CardChoiceBufferIDs = append(player.CardChoiceBufferIDs, pCard0, pCard1)
	err = storage.PutPlayer(player)
	if err != nil {
		return []string{}, err
	}
	log.Printf("drafted cards %s and %s to project selection buffer for player %s", pCard0, pCard1, playerID)
	return []string{pCard0, pCard1}, nil
}

func AcceptProjectCard(storage storage.Storage, gameID string, playerID string, cardID string) (model.Player, error) {
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a pool", playerID)
	}
	if !utils.ContainsString(player.CardChoiceBufferIDs, cardID) {
		return model.Player{}, fmt.Errorf("player %s does not have card %s in his buffer", playerID, cardID)
	}
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	stack, exists := storage.GetStackByID(game.ProjectStackID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not have a project stack", gameID)
	}
	card, exists := storage.GetCardByID(cardID)
	if !exists {
		return model.Player{}, fmt.Errorf("card %s does not exist", cardID)
	}
	project := model.Project{
		ID:              utils.CreateUUID(),
		CardID:          cardID,
		PlayerID:        playerID,
		ResourceCardIDs: []string{},
		Colors:          card.Colors,
		Payout:          card.Payout,
		EffectID:        card.EffectID,
		EffectArgs:      card.EffectArgs,
	}
	err := storage.PutProject(project)
	if err != nil {
		return model.Player{}, err
	}
	player.ProjectIDs = append(player.ProjectIDs, project.ID)
	player.CardChoiceBufferIDs = removeStringElement(player.CardChoiceBufferIDs, cardID)
	stack.CardIDs = append(stack.CardIDs, player.CardChoiceBufferIDs...)
	player.CardChoiceBufferIDs = []string{}
	err = storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutStack(stack)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("accepted card %s from player %s to project stack %s", cardID, playerID, stack.ID)
	return player, nil
}

func EndTurn(storage storage.Storage, gameID string, playerID string) (model.Game, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Game{}, fmt.Errorf("game %s does not exist", gameID)
	}
	currentPlayerPosition := utils.IndexOf(game.CurrentPlayerID, game.PlayerIDs)
	if currentPlayerPosition == -1 {
		return model.Game{}, fmt.Errorf("current player %s not in game %s", game.CurrentPlayerID, gameID)
	}
	nextPlayerPosition := (currentPlayerPosition + 1) % len(game.PlayerIDs)
	game.CurrentPlayerID = game.PlayerIDs[nextPlayerPosition]
	log.Printf("ended turn for player %s, new current player is %s", playerID, game.CurrentPlayerID)
	err := storage.PutGame(game)
	if err != nil {
		return model.Game{}, err
	}
	player, exists := storage.GetPlayerByID(game.CurrentPlayerID)
	if !exists {
		return model.Game{}, fmt.Errorf("current player %s does not exist", game.CurrentPlayerID)
	}
	// apply event effect if present
	eventCard, exists := storage.GetCardByID(game.CurrentEventCardID)
	if !exists {
		return model.Game{}, fmt.Errorf("event card %s does not exist", game.CurrentEventCardID)
	}
	if eventCard.EffectID != "" {
		_, err = applyEffect(storage, ScopeBeginningOfTurn, eventCard.EffectID, eventCard.EffectArgs, game, player, model.Project{})
		if err != nil {
			return model.Game{}, err
		}
	}
	LogAction(storage, gameID, "Next Turn", model.ActionNextTurn, playerID, "", nil);
	return game, nil
}

func EndGame(storage storage.Storage, gameID string) (model.GameResult, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.GameResult{}, fmt.Errorf("game %s does not exist", gameID)
	}
	game.Status = model.GameStateEnded
	credits := []int{}
	highestCredit := 0
	winningPlayerID := ""
	for _, playerID := range game.PlayerIDs {
		player, exists := storage.GetPlayerByID(playerID)
		if !exists {
			return model.GameResult{}, fmt.Errorf("player %s does not exist", playerID)
		}
		credits = append(credits, player.Credits)
		if player.Credits > highestCredit {
			highestCredit = player.Credits
			winningPlayerID = playerID
		}
		// FIXME: what happens on a tie?
	}		
	log.Printf("ended game %s, winner is player %s", gameID, winningPlayerID)
	err := storage.PutGame(game)
	if err != nil {
		return model.GameResult{}, err
	}
	LogAction(storage, gameID, "Game ended", model.ActionGameEnded, winningPlayerID, "", nil);
	return model.GameResult{
		ID:             utils.CreateUUID(),
		PlayerIDs:      game.PlayerIDs,
		WinnerPlayerID: winningPlayerID,
		Credits:        credits,
	}, nil
}

func MoveResourceCardFromHandToProject(storage storage.Storage, gameID string, playerID string, projectID string, resourceCardID string) (model.Player, error) {
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a pool", playerID)
	}
	project, exists := storage.GetProjectByID(projectID)
	if !exists {
		return model.Player{}, fmt.Errorf("project %s does not exist", projectID)
	}
	if !utils.ContainsString(player.ProjectIDs, projectID) {
		return model.Player{}, fmt.Errorf("player %s does not have project %s in his projects", playerID, projectID)
	}
	card, exists := storage.GetCardByID(resourceCardID)
	if !exists {
		return model.Player{}, fmt.Errorf("card %s does not exist", resourceCardID)
	}
	if len(card.Colors) != 1 || !utils.ContainsString(project.Colors, card.Colors[0]) {
		return model.Player{}, fmt.Errorf("card %s does not have color %s in project %s", resourceCardID, card.Colors, projectID)
	}
	hand, exists := storage.GetHandByID(player.HandID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a hand", playerID)
	}
	hand.CardIDs = removeStringElement(hand.CardIDs, resourceCardID)
	project.ResourceCardIDs = append(project.ResourceCardIDs, resourceCardID)
	err := storage.PutHand(hand)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutProject(project)
	if err != nil {
		return model.Player{}, err
	}
	player, err = applyResourcePlayEffect(storage, gameID, playerID, projectID, resourceCardID, ScopePlayedCardToProject)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("moved card %s from player %s hand to project %s", resourceCardID, playerID, projectID)
	return player, nil
}

func SellProject(storage storage.Storage, gameID string, playerID string, projectID string) (model.Player, error) {
	game, exists := storage.GetGameByID(gameID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not exist", gameID)
	}
	player, exists := storage.GetPlayerByID(playerID)
	if !exists {
		return model.Player{}, fmt.Errorf("player %s does not have a pool", playerID)
	}
	project, exists := storage.GetProjectByID(projectID)
	if !exists {
		return model.Player{}, fmt.Errorf("project %s does not exist", projectID)
	}
	if !utils.ContainsString(player.ProjectIDs, projectID) {
		return model.Player{}, fmt.Errorf("player %s does not have project %s in his projects", playerID, projectID)
	}
	projectDiscardStack, exists := storage.GetStackByID(game.ProjectDiscardStackID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not have a project discard stack", gameID)
	}
	resourceDiscardStack, exists := storage.GetStackByID(game.ResourceDiscardStackID)
	if !exists {
		return model.Player{}, fmt.Errorf("game %s does not have a resource discard stack", gameID)
	}
	player.ProjectIDs = removeStringElement(player.ProjectIDs, projectID)
	projectDiscardStack.CardIDs = append(projectDiscardStack.CardIDs, project.CardID)
	resourceDiscardStack.CardIDs = append(resourceDiscardStack.CardIDs, project.ResourceCardIDs...)
	err := storage.PutPlayer(player)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutStack(projectDiscardStack)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutStack(resourceDiscardStack)
	if err != nil {
		return model.Player{}, err
	}
	player, err = applyProjectSellEffect(storage, gameID, playerID, projectID)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.RemoveProject(project)
	if err != nil {
		return model.Player{}, err
	}
	log.Printf("player %s sold project %s", playerID, projectID)
	return player, nil
}

func MoveResourceCardToPlayer(storage storage.Storage, gameID string, sourcePlayerID string, cardID, targetPlayerID string) (model.Player, error) {
	sourcePlayer, exists := storage.GetPlayerByID(sourcePlayerID)
	if !exists {
		return model.Player{}, fmt.Errorf("source player %s does exist", sourcePlayerID)
	}
	targetPlayer, exists := storage.GetPlayerByID(targetPlayerID)
	if !exists {
		return model.Player{}, fmt.Errorf("source player %s does exist", sourcePlayerID)
	}
	sourceHand, exists := storage.GetHandByID(sourcePlayer.HandID)
	if !exists {
		return model.Player{}, fmt.Errorf("source player %s does not have a hand", sourcePlayerID)
	}
	targetHand, exists := storage.GetHandByID(targetPlayer.HandID)
	if !exists {
		return model.Player{}, fmt.Errorf("target player %s does not have a hand", targetPlayerID)
	}
	if !utils.ContainsString(sourceHand.CardIDs, cardID) {
		return model.Player{}, fmt.Errorf("source player %s does not have card %s in his hand", sourcePlayerID, cardID)
	}
	sourceHand.CardIDs = removeStringElement(sourceHand.CardIDs, cardID)
	targetHand.CardIDs = append(targetHand.CardIDs, cardID)
	err := storage.PutHand(sourceHand)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutHand(targetHand)
	if err != nil {
		return model.Player{}, err
	}
	return model.Player{}, nil
}

func MoveCreditsToPlayer(storage storage.Storage, gameID string, sourcePlayerID string, creditsVal int, targetPlayerID string) (model.Player, error) {
	sourcePlayer, exists := storage.GetPlayerByID(sourcePlayerID)
	if !exists {
		return model.Player{}, fmt.Errorf("source player %s does exist", sourcePlayerID)
	}
	targetPlayer, exists := storage.GetPlayerByID(targetPlayerID)
	if !exists {
		return model.Player{}, fmt.Errorf("source player %s does exist", sourcePlayerID)
	}
	if sourcePlayer.Credits < creditsVal {
		return model.Player{}, fmt.Errorf("source player %s does not have enough credits", sourcePlayerID)
	}
	sourcePlayer.Credits -= creditsVal
	targetPlayer.Credits += creditsVal
	err := storage.PutPlayer(sourcePlayer)
	if err != nil {
		return model.Player{}, err
	}
	err = storage.PutPlayer(targetPlayer)
	if err != nil {
		return model.Player{}, err
	}
	return sourcePlayer, nil
}

func LogAction(storage storage.Storage, gameID string, name string, actionType string, sourceID string, targetID string, objectIDs []string) (model.Action, error) {
	action := model.Action{
		ID:        utils.CreateUUID(),
		Timestamp: time.Now().Unix(),
		Type:      actionType,
		SourceID:  sourceID,
		TargetID:  targetID,
		ObjectIDs: objectIDs,
		Name:      name,
	}
	err := storage.PutAction(action)
	if err != nil {
		return model.Action{}, err
	}
	return action, nil
}
