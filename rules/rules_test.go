/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package rules_test

import (
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/rules"
	"gitlab.com/projectlaunchpad/backend/storage"
)

func isCardOfType(storage storage.Storage, cardID string, cardType string) bool {
	if cardID == "" || cardType == "" {
		return false
	}
	card, exists := storage.GetCardByID(cardID)
	if !exists {
		return false
	}
	return card.Type == cardType
}

func TestRules(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	// starting from a clean database
	os.Remove(storage.DatabaseFile)
	storageInst, err := storage.NewStorage(config)
	require.NoError(t, err)
	require.NotNil(t, storageInst)
	defer func() {
		storageInst.Close()
		os.Remove(storage.DatabaseFile)
	}()

	t.Run("create game", func(t *testing.T) {
		player0Name := "player0name"
		player, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		// player integrity
		require.NotEmpty(t, player.ID)
		require.NotEmpty(t, player.GameID)
		require.NotEmpty(t, player, player.SessionToken)
		require.Equal(t, player0Name, player.Name)
		require.True(t, player.IsFirstPlayer)
		require.Equal(t, rules.InitialCredits, player.Credits)
		require.Empty(t, player.CardChoiceBufferIDs)

		// cards integrity
		require.NotEmpty(t, player, player.PersonaCardID)
		require.True(t, isCardOfType(*storageInst, player.PersonaCardID, model.CardTypePersona))
		require.NotEmpty(t, player, player.HandID)
		hand, exists := storageInst.GetHandByID(player.HandID)
		require.True(t, exists)
		require.Len(t, hand.CardIDs, 3)
		require.NotEmpty(t, player, player.ProjectIDs)
		require.Len(t, player.ProjectIDs, 2)

		// game integrity
		game, exists := storageInst.GetGameByID(player.GameID)
		require.True(t, exists)
		require.NotEmpty(t, game.ID)
		require.Equal(t, game.Status, model.GameStateNew)
		require.NotEmpty(t, game.InviteCode)
		require.Len(t, game.PlayerIDs, 1)
		require.Equal(t, player.ID, game.PlayerIDs[0])
		require.NotEmpty(t, game.PoolStackID)
		pool, exists := storageInst.GetStackByID(game.PoolStackID)
		require.True(t, exists)
		require.Len(t, pool.CardIDs, 1)
		require.True(t, isCardOfType(*storageInst, pool.CardIDs[0], model.CardTypeResource))
		require.NotEmpty(t, game.ResourceStackID)
		require.NotEmpty(t, game.ResourceDiscardStackID)
		require.NotEmpty(t, game.ProjectStackID)
		require.NotEmpty(t, game.ProjectDiscardStackID)
		require.NotEmpty(t, game.EventStackID)
		require.NotEmpty(t, game.EventDiscardStackID)
		require.NotEmpty(t, game.PersonaStackID)
		require.NotEmpty(t, game.PersonaDiscardStackID)
		require.Empty(t, game.CurrentEventCardID)
		require.Empty(t, game.ActionIDs)
		require.Empty(t, game.CurrentPlayerID)
		require.Equal(t, player.ID, game.FirstPlayerID)
	})

	t.Run("join game", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		player1Name := "player1name"
		player1, err := rules.JoinGame(*storageInst, player0.GameID, player1Name)
		require.NoError(t, err)
		require.Equal(t, player1.Name, player1Name)
		require.Equal(t, player1.GameID, player0.GameID)
		player2Name := "player2name"
		player2, err := rules.JoinGame(*storageInst, player0.GameID, player2Name)
		require.NoError(t, err)
		require.Equal(t, player2.Name, player2Name)
		require.Equal(t, player2.GameID, player0.GameID)
		player3Name := "player3name"
		player3, err := rules.JoinGame(*storageInst, player0.GameID, player3Name)
		require.NoError(t, err)
		require.Equal(t, player3.Name, player3Name)
		require.Equal(t, player3.GameID, player0.GameID)
		game, exists = storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		require.Len(t, game.PlayerIDs, 4)
		// add more players than allowed
		player4Name := "player4name"
		_, err = rules.JoinGame(*storageInst, player0.GameID, player4Name)
		require.Error(t, err)
		require.Len(t, game.PlayerIDs, 4)
		dbGame, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		require.Len(t, dbGame.PlayerIDs, 4)
	})

	t.Run("start game", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		_, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		_, err = rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		dbGame, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		require.Equal(t, model.GameStateRunning, dbGame.Status)
		// try to join a game that is already running
		player1Name := "player1name"
		_, err = rules.JoinGame(*storageInst, player1Name, player0.GameID)
		require.Error(t, err)
	})

	t.Run("add resource card from deck to hand", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		_, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		_, err = rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		dbGame, exists := storageInst.GetGameByID(player0.GameID)
		require.True(t, exists)
		require.Equal(t, model.GameStateRunning, dbGame.Status)

		// add resource card to hand
		cardID, err := rules.DrawResourceCard(*storageInst, player0.GameID)
		require.NoError(t, err)
		dbStack, _ := storageInst.GetStackByID(dbGame.ResourceStackID)
		require.NotContains(t, dbStack.CardIDs, cardID)
		hand, _ := storageInst.GetHandByID(player0.HandID)
		require.NotContains(t, hand.CardIDs, cardID)
		player, err := rules.AddResouceCardToPlayerHand(*storageInst, player0.GameID, player0.ID, cardID)
		require.NoError(t, err)
		require.Equal(t, player0.ID, player.ID)
		hand, _ = storageInst.GetHandByID(player0.HandID)
		require.Contains(t, hand.CardIDs, cardID)
	})

	t.Run("get resource card from pool", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		dbStack, _ := storageInst.GetStackByID(game.PoolStackID)
		expectedFirstCardID := dbStack.CardIDs[0]
		cardID, err := rules.GetResourceCardFromPool(*storageInst, game.ID)
		require.NoError(t, err)
		require.Equal(t, expectedFirstCardID, cardID)
		dbStack, _ = storageInst.GetStackByID(game.PoolStackID)
		require.NotContains(t, dbStack.CardIDs, cardID)
	})

	t.Run("put resource card to pool", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		expectedCardID, err := rules.DrawResourceCard(*storageInst, player0.GameID)
		require.NoError(t, err)
		dbStack, _ := storageInst.GetStackByID(game.PoolStackID)
		expectedLength := len(dbStack.CardIDs) + 1
		_, err = rules.AddResourceCardToPool(*storageInst, game.ID, expectedCardID)
		require.NoError(t, err)
		dbStack, _ = storageInst.GetStackByID(game.PoolStackID)
		require.Equal(t, dbStack.CardIDs[0], expectedCardID)
		require.Len(t, dbStack.CardIDs, expectedLength)
	})

	t.Run("acquire project card", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		require.Empty(t, player0.CardChoiceBufferIDs)
		dbStack, exists := storageInst.GetStackByID(game.ProjectStackID)
		require.True(t, exists)
		expectedPCardsCount := len(dbStack.CardIDs) - 2
		pcardChoices, err := rules.AquireProjectCard(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		require.Len(t, pcardChoices, 2)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Len(t, dbPlayer.CardChoiceBufferIDs, 2)
		dbGame, exists := storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		dbStack, exists = storageInst.GetStackByID(dbGame.ProjectStackID)
		require.True(t, exists)
		require.Len(t, dbStack.CardIDs, expectedPCardsCount)
	})

	t.Run("accept project card", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		pcardChoices, err := rules.AquireProjectCard(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		dbStack, exists := storageInst.GetStackByID(game.ProjectStackID)
		require.True(t, exists)
		expectedPCardsCount := len(dbStack.CardIDs) + 1
		expectedPlayerProjectCount := len(player0.ProjectIDs) + 1
		player, err := rules.AcceptProjectCard(*storageInst, game.ID, player0.ID, pcardChoices[0])
		require.NoError(t, err)
		require.Empty(t, player.CardChoiceBufferIDs)
		player, exists = storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Len(t, player.ProjectIDs, expectedPlayerProjectCount)
		require.Len(t, player.CardChoiceBufferIDs, 0)
		dbStack, exists = storageInst.GetStackByID(game.ProjectStackID)
		require.True(t, exists)
		require.Len(t, dbStack.CardIDs, expectedPCardsCount)
	})

	t.Run("end turn", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		player1Name := "player1name"
		player1, err := rules.JoinGame(*storageInst, player0.GameID, player1Name)
		require.NoError(t, err)
		player2Name := "player2name"
		player2, err := rules.JoinGame(*storageInst, player0.GameID, player2Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		dbGame, exists := storageInst.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, dbGame.CurrentPlayerID, player0.ID)
		game, err = rules.EndTurn(*storageInst, game.ID, player0.ID)
		require.NoError(t, err)
		require.Equal(t, game.CurrentPlayerID, player1.ID)
		game, err = rules.EndTurn(*storageInst, game.ID, player0.ID)
		require.NoError(t, err)
		require.Equal(t, game.CurrentPlayerID, player2.ID)
		game, err = rules.EndTurn(*storageInst, game.ID, player0.ID)
		require.NoError(t, err)
		require.Equal(t, game.CurrentPlayerID, player0.ID)
	})

	t.Run("Add resource card to project", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		require.Equal(t, 3, player0.Credits)

		// create custom resource card - matching color
		resourceCard0 := model.Card{
			ID:             "test-resource-to-project-resourcecard-0",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"blue"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard0)
		require.NoError(t, err)

		// create custom resource card - not matching color
		resourceCard1 := model.Card{
			ID:             "test-resource-to-project-resourcecard-1",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"green"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard1)
		require.NoError(t, err)

		// create custom project
		project := model.Project{
			ID:              "test-resource-to-project-project-0",
			CardID:          "",
			PlayerID:        "",
			ResourceCardIDs: []string{},
			Colors:          []string{"blue"},
			Payout:          map[int]int{},
			EffectID:        "",
			EffectArgs:      []string{},
		}
		err = storageInst.PutProject(project)
		require.NoError(t, err)

		// add cards and project to player
		hand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		hand.CardIDs = append(hand.CardIDs, resourceCard0.ID)
		hand.CardIDs = append(hand.CardIDs, resourceCard1.ID)
		err = storageInst.PutHand(hand)
		require.NoError(t, err)
		player0.ProjectIDs = []string{project.ID}
		err = storageInst.PutPlayer(player0)
		require.NoError(t, err)

		// remove drawn effects card, replace by no effect
		setEvent(storageInst, game, "", []string{})

		// execute action - not matching color
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project.ID, resourceCard1.ID)
		require.Error(t, err)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		dbHand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		dbProject, exists := storageInst.GetProjectByID(dbPlayer.ProjectIDs[0])
		require.True(t, exists)
		require.Len(t, dbHand.CardIDs, rules.InitialHandSize+2)
		require.Contains(t, dbHand.CardIDs, resourceCard1.ID)
		require.Len(t, dbProject.ResourceCardIDs, 0)
		require.Equal(t, 3, dbPlayer.Credits)

		// execute action - matching color
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project.ID, resourceCard0.ID)
		require.NoError(t, err)

		// check result
		dbPlayer, exists = storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		dbHand, exists = storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		dbProject, exists = storageInst.GetProjectByID(dbPlayer.ProjectIDs[0])
		require.True(t, exists)
		require.Len(t, dbHand.CardIDs, rules.InitialHandSize+1)
		require.NotContains(t, dbHand.CardIDs, resourceCard0.ID)
		require.Len(t, dbProject.ResourceCardIDs, 1)
		require.Contains(t, dbProject.ResourceCardIDs, resourceCard0.ID)
	})

	t.Run("sell project", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		// create custom resource card 0
		resourceCard0 := model.Card{
			ID:             "test-sell-project-resourcecard-0",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"blue"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard0)
		require.NoError(t, err)

		// create custom resource card 1
		resourceCard1 := model.Card{
			ID:             "test-sell-project-resourcecard-1",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"blue"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard1)
		require.NoError(t, err)

		// create custom resource card 2
		resourceCard2 := model.Card{
			ID:             "test-sell-project-resourcecard-2",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"green"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard2)
		require.NoError(t, err)

		// create custom project
		project := model.Project{
			ID:              "test-sell-project-project-0",
			CardID:          "",
			PlayerID:        "",
			ResourceCardIDs: []string{},
			Colors:          []string{"blue", "green"},
			Payout: map[int]int{
				1: 1,
				2: 2,
				3: 3,
			},
			EffectID:   "",
			EffectArgs: []string{},
		}
		err = storageInst.PutProject(project)
		require.NoError(t, err)

		// add cards and project to player
		hand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		hand.CardIDs = append(hand.CardIDs, resourceCard0.ID)
		hand.CardIDs = append(hand.CardIDs, resourceCard1.ID)
		hand.CardIDs = append(hand.CardIDs, resourceCard2.ID)
		err = storageInst.PutHand(hand)
		require.NoError(t, err)
		player0.ProjectIDs = []string{project.ID}
		player0.Credits = 3
		err = storageInst.PutPlayer(player0)
		require.NoError(t, err)
		dbHand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		require.Len(t, dbHand.CardIDs, rules.InitialHandSize+3)

		// remove drawn effects card, replace by no effect
		setEvent(storageInst, game, "", []string{})

		// add cards to project
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project.ID, resourceCard0.ID)
		require.NoError(t, err)
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project.ID, resourceCard1.ID)
		require.NoError(t, err)
		_, err = rules.MoveResourceCardFromHandToProject(*storageInst, game.ID, player0.ID, project.ID, resourceCard2.ID)
		require.NoError(t, err)

		dbHand, exists = storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		require.Len(t, dbHand.CardIDs, rules.InitialHandSize)
		dbPlayer, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, 0, dbPlayer.Credits)

		// execute action
		player, err := rules.SellProject(*storageInst, game.ID, player0.ID, project.ID)
		require.NoError(t, err)

		// check result
		dbPlayer, exists = storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		dbHand, exists = storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		require.Len(t, dbHand.CardIDs, rules.InitialHandSize)
		require.NotContains(t, dbHand.CardIDs, resourceCard0.ID)
		require.NotContains(t, dbHand.CardIDs, resourceCard1.ID)
		require.NotContains(t, dbHand.CardIDs, resourceCard2.ID)

		// check db
		require.Len(t, dbPlayer.ProjectIDs, 0)
		require.Equal(t, 3, player0.Credits)

		// check returns
		require.Len(t, player.ProjectIDs, 0)
		require.Equal(t, 3, player.Credits)
	})

	t.Run("move resource card to player", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		player1Name := "player1name"
		player1, err := rules.JoinGame(*storageInst, player0.GameID, player1Name)
		require.NoError(t, err)
		game, err := rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)

		// create custom resource card 0
		resourceCard0 := model.Card{
			ID:             "test-move-resourcecard-resourcecard-0",
			Type:           model.CardTypeResource,
			Name:           []string{},
			Description:    []string{},
			IllustrationID: "",
			IconID:         "",
			EffectID:       "",
			EffectArgs:     []string{},
			Colors:         []string{"blue"},
			Cost:           1,
			Payout:         map[int]int{},
		}
		err = storageInst.PutCard(resourceCard0)
		require.NoError(t, err)
		sourceHand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		sourceHand.CardIDs = append(sourceHand.CardIDs, resourceCard0.ID)
		err = storageInst.PutHand(sourceHand)
		require.NoError(t, err)

		// execute action
		_, err = rules.MoveResourceCardToPlayer(*storageInst, game.ID, player0.ID, resourceCard0.ID, player1.ID)
		require.NoError(t, err)

		// check result
		dbSourceHand, exists := storageInst.GetHandByID(player0.HandID)
		require.True(t, exists)
		dbTargetHand, exists := storageInst.GetHandByID(player1.HandID)
		require.True(t, exists)
		require.Len(t, dbSourceHand.CardIDs, rules.InitialHandSize)
		require.Len(t, dbTargetHand.CardIDs, rules.InitialHandSize+1)
		require.NotContains(t, dbSourceHand.CardIDs, resourceCard0.ID)
		require.Contains(t, dbTargetHand.CardIDs, resourceCard0.ID)
	})

	t.Run("move credits to player", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := rules.CreateGame(*storageInst, player0Name)
		require.NoError(t, err)
		player1Name := "player1name"
		player1, err := rules.JoinGame(*storageInst, player0.GameID, player1Name)
		require.NoError(t, err)
		_, err = rules.StartGame(*storageInst, player0.GameID, player0.ID)
		require.NoError(t, err)
		require.Equal(t, rules.InitialCredits, player0.Credits)
		require.Equal(t, rules.InitialCredits, player1.Credits)

		// execute action
		player, err := rules.MoveCreditsToPlayer(*storageInst, player0.GameID, player0.ID, 2, player1.ID)
		require.NoError(t, err)

		// check db
		dbPlayer0, exists := storageInst.GetPlayerByID(player0.ID)
		require.True(t, exists)
		require.Equal(t, rules.InitialCredits-2, dbPlayer0.Credits)
		dbPlayer1, exists := storageInst.GetPlayerByID(player1.ID)
		require.True(t, exists)
		require.Equal(t, rules.InitialCredits+2, dbPlayer1.Credits)

		// check returns
		require.Equal(t, rules.InitialCredits-2, player.Credits)
	})
}
