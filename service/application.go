/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package service

import (
	"log"
	"net/http"
	"path/filepath"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"gitlab.com/projectlaunchpad/backend/endpoints"
	"gitlab.com/projectlaunchpad/backend/static"
	"gitlab.com/projectlaunchpad/backend/storage"
	"gitlab.com/projectlaunchpad/backend/utils"
)

// Application represents the main application struct.
type Application struct {
	config  *viper.Viper
	storage *storage.Storage
	Engine  *gin.Engine
}

// StaticHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type StaticHandler struct {
	Assets http.FileSystem
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h StaticHandler) ServeHTTP(ctx *gin.Context) {
	// Get the absolute path to prevent directory traversal
	path, err := filepath.Abs(ctx.Request.URL.Path)
	if err != nil {
		// No absolute path, respond with a 400 bad request and stop
		http.Error(ctx.Writer, err.Error(), http.StatusBadRequest)
		return
	}
	// Check if the file exists in the assets.
	_, err = h.Assets.Open(path)
	if err != nil {
		// File does not exist, redirect to index.
		log.Printf("File %s does not exist.", path)
		http.Redirect(ctx.Writer, ctx.Request, "/index.html", http.StatusSeeOther)
		return
	}

	// Otherwise, use http.FileServer to serve the static dir.
	http.FileServer(h.Assets).ServeHTTP(ctx.Writer, ctx.Request)
}

// AuthRequired authenticates a request.
func AuthRequired(storage *storage.Storage) gin.HandlerFunc {
	return func(c *gin.Context) {
		queryParams := c.Request.URL.Query()
		playerID := c.Request.Header.Get("Launchpad-Player")
		token := c.Request.Header.Get("Launchpad-Token")
		if playerID == "" && len(queryParams["Launchpad-Player"]) > 0 {
			playerID = queryParams["Launchpad-Player"][0]
		} else if playerID == "" && len(queryParams["Launchpad-Player"]) < 1 {
			log.Println("playerID is missing in request")
			c.Abort()
			c.String(http.StatusForbidden, "the header 'Launchpad-Player' is missing")
			return
		}
		if token == "" && len(queryParams["Launchpad-Token"]) > 0 {
			token = queryParams["Launchpad-Token"][0]
		} else if token == "" && len(queryParams["Launchpad-Token"]) < 1 {
			log.Println("token is missing in request")
			c.Abort()
			c.String(http.StatusForbidden, "the header 'Launchpad-Token' is missing")
			return
		}
		player, exists := storage.GetPlayerByID(playerID)
		if !exists || player.SessionToken != token {
			log.Printf("player not found or token verification failed for token %s and player ID %s", token, playerID)
			c.Abort()
			c.String(http.StatusForbidden, "authentication failed")
			return
		}
		gameID := c.Param("gameid")
		game, exists := storage.GetGameByID(gameID)
		if !exists || !utils.ContainsString(game.PlayerIDs, playerID) {
			log.Printf("player %s not part of game %s", playerID, gameID)
			c.Abort()
			c.String(http.StatusForbidden, "authentication failed")
			return
		}
		// store the authenticated account in the context.
		c.Set("player", player)
		c.Set("game", game)
		c.Next()
	}
}

// NewApplication creates a new application instance.
func NewApplication(config *viper.Viper) (*Application, error) {
	var err error

	app := &Application{}
	app.config = config

	// Create the Gin instance.
	gin.SetMode(gin.ReleaseMode)
	app.Engine = gin.New()

	// Logger middleware will write the logs to gin.DefaultWriter.
	app.Engine.Use(gin.Logger())

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	app.Engine.Use(gin.Recovery())

	// CORS settings
	app.Engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		AllowCredentials: true,
	}))

	// create storage
	app.storage, err = storage.NewStorage(config)
	if err != nil {
		log.Panic(err)
	}

	// Creating public endpoint instances - add new endpoints here.
	statusEndpoint, err := endpoints.NewStatusEndpoint(config, app.storage)
	if err != nil {
		log.Panic(err)
	}
	gameEndpoint, err := endpoints.NewGameEndpoint(config, app.storage)
	if err != nil {
		log.Panic(err)
	}

	// system operations
	app.Engine.GET("/status", statusEndpoint.GetStatus) // returns status

	// returns player
	app.Engine.GET("/game/create/:playername", gameEndpoint.CreateGame)
	// returns game
	app.Engine.GET("/game/join/:invitecode/:playername", gameEndpoint.JoinGame)
	// returns json of all cards
	app.Engine.GET("/data/cards", gameEndpoint.GetAllCards)

	// data queries, restricted

	// returns stack
	app.Engine.GET("/game/data/stack/:gameid/:stackid", AuthRequired(app.storage), gameEndpoint.GetStack)
	// returns hand
	app.Engine.GET("/game/data/hand/:gameid/:handid", AuthRequired(app.storage), gameEndpoint.GetHand)
	// returns player
	app.Engine.GET("/game/data/player/:gameid/:playerid", AuthRequired(app.storage), gameEndpoint.GetPlayer)
	// returns project
	app.Engine.GET("/game/data/project/:gameid/:projectid", AuthRequired(app.storage), gameEndpoint.GetProject)
	// returns action
	app.Engine.GET("/game/data/action/:gameid/:actionid", AuthRequired(app.storage), gameEndpoint.GetAction)

	// player actions, restricted

	// returns game
	app.Engine.GET("/game/start/:gameid", AuthRequired(app.storage), gameEndpoint.StartGame)
	// returns game
	app.Engine.GET("/game/state/:gameid", AuthRequired(app.storage), gameEndpoint.GetGameState)

	// returns card
	app.Engine.GET("/action/:gameid/resource/draw", AuthRequired(app.storage), gameEndpoint.DrawResourceCardFromDrawDeck)

	// returns array of cards
	app.Engine.GET("/action/:gameid/project/draw", AuthRequired(app.storage), gameEndpoint.AquireProjectCard)
	// returns player
	app.Engine.PUT("/action/:gameid/project/accept/:projectcardid", AuthRequired(app.storage), gameEndpoint.AcceptProjectCard)

	// returns card
	app.Engine.GET("/action/:gameid/pool/acquire", AuthRequired(app.storage), gameEndpoint.GetResourceCardFromPool)
	// returns game
	app.Engine.PUT("/action/:gameid/pool/:resourcecardid", AuthRequired(app.storage), gameEndpoint.DiscardResourceCardToPool)

	// returns player
	app.Engine.PUT("/action/:gameid/add/:projectid/:resourcecardid", AuthRequired(app.storage), gameEndpoint.MoveResourceCardFromHandToProject)

	// returns player
	app.Engine.PUT("/action/:gameid/sell/:projectid", AuthRequired(app.storage), gameEndpoint.SellProject)

	// returns game
	app.Engine.PUT("/action/:gameid/turn", AuthRequired(app.storage), gameEndpoint.TurnComplete)

	// returns player
	app.Engine.PUT("/action/:gameid/givecard/:resourcecardid/:playerid", AuthRequired(app.storage), gameEndpoint.GiveResourceCardToPlayer)
	// returns player
	app.Engine.PUT("/action/:gameid/givecredits/:creditsvalue/:playerid", AuthRequired(app.storage), gameEndpoint.GiveCreditsToPlayer)

	// returns game
	app.Engine.PUT("/action/:gameid/end", AuthRequired(app.storage), gameEndpoint.EndGame)

	// Create the route for static content, served from /
	static := StaticHandler{Assets: static.Assets}
	// capturing all non-matching routes, assuming them to be static content
	app.Engine.NoRoute(static.ServeHTTP)

	return app, nil
}

// Close cleans up and closes all resources.
func (app *Application) Close() {
}

// GetHandler returns the application handler instance.
func (app *Application) GetHandler() http.Handler {
	return app.Engine
}

// Start starts the service handler.
func (app *Application) Start() {
	// Listen and serve.
	log.Printf("listening on http address %s\n", app.config.GetString("httpurl"))
	defer app.storage.Close()
	err := app.Engine.Run(app.config.GetString("httpurl"))
	if err != nil {
		log.Panic(err)
	}
}
