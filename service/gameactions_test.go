/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package service_test

import (
	"crypto/tls"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/service"
	"gitlab.com/projectlaunchpad/backend/storage"
)

func TestGameActionsEndpoint(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	app, err := service.NewApplication(config)
	require.NoError(t, err)
	defer func() {
		os.Remove(storage.DatabaseFile)
		require.NoError(t, err)
	}()

	ts := httptest.NewServer(app.Engine)
	defer ts.Close()

	cookieJar, _ := cookiejar.New(nil)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr,
		Jar: cookieJar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}

	t.Run("get status", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodGet, ts.URL+"/status", nil)
		require.NoError(t, err)

		resp, err := client.Do(req)
		require.NoError(t, err)

		require.Equal(t, 200, resp.StatusCode)
	})

	/*
		t.Run("some POST query with urlencode", func(t *testing.T) {
			params := url.Values{}
			params.Set("key", "value")
			req, err := http.NewRequest(http.MethodPost, ts.URL+"/endpointName", strings.NewReader(params.Encode()))
			require.NoError(t, err)
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
			req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

			resp, err := client.Do(req)
			require.NoError(t, err)

			require.Equal(t, 200, resp.StatusCode)
		})

		t.Run("PUT object", func(t *testing.T) {

			someObject := &some.Object{
				ID:             "0",
				Type:           "type",
			}
			bodyBytes, err := json.Marshal(someObject)
			require.NoError(t, err)

			req, err := http.NewRequest(http.MethodPut, ts.URL+"/endpointName/someMorePath", bytes.NewReader(bodyBytes))
			require.NoError(t, err)

			req.Header.Add("Launchpad-Auth", "sessionToken")

			resp, err := client.Do(req)
			require.NoError(t, err)
			require.Equal(t, 200, resp.StatusCode)
		})
	*/
}
