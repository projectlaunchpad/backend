/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package service_test

import (
	"crypto/tls"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/service"
	"gitlab.com/projectlaunchpad/backend/storage"
)

func TestStartJoinGameEndpoint(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	app, err := service.NewApplication(config)
	require.NoError(t, err)
	defer func() {
		os.Remove(storage.DatabaseFile)
		require.NoError(t, err)
	}()

	ts := httptest.NewServer(app.Engine)
	defer ts.Close()

	cookieJar, _ := cookiejar.New(nil)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr,
		Jar: cookieJar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}

	storage := storage.GetStorage()
	require.NotNil(t, storage)

	t.Run("create game", func(t *testing.T) {
		playerName := "player0name"
		player, err := createGame(client, ts.URL, playerName)
		require.NoError(t, err)
		require.Equal(t, playerName, player.Name)

		dbGame, exists := storage.GetGameByID(player.GameID)
		require.True(t, exists)
		require.Equal(t, player.ID, dbGame.PlayerIDs[0])
		require.Equal(t, model.GameStateNew, dbGame.Status)
	})

	t.Run("join game", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := createGame(client, ts.URL, player0Name)
		require.NoError(t, err)
		require.Equal(t, player0Name, player0.Name)

		game, err := getGameForPlayer(client, ts.URL, player0)
		require.NoError(t, err)
		require.Equal(t, player0.GameID, game.ID)

		// try with wrong invite code
		player1Name := "player1name"
		player1, err := joinGame(client, ts.URL, game.InviteCode+"XX", player1Name)
		require.Error(t, err)

		// try with correct invite code
		player1, err = joinGame(client, ts.URL, game.InviteCode, player1Name)
		require.NoError(t, err)
		require.Equal(t, player1.GameID, game.ID)

		// third player
		player2Name := "player2name"
		player2, err := joinGame(client, ts.URL, game.InviteCode, player2Name)
		require.NoError(t, err)
		require.Equal(t, player2.GameID, game.ID)

		// fourth player
		player3Name := "player3name"
		player3, err := joinGame(client, ts.URL, game.InviteCode, player3Name)
		require.NoError(t, err)
		require.Equal(t, player3.GameID, game.ID)

		// fifth player, this needs to fail
		player4Name := "player4name"
		_, err = joinGame(client, ts.URL, game.InviteCode, player4Name)
		require.Error(t, err)

		dbGame, exists := storage.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, 4, len(dbGame.PlayerIDs))
	})

	t.Run("start game", func(t *testing.T) {
		player0Name := "player0name"
		player0, err := createGame(client, ts.URL, player0Name)
		require.NoError(t, err)
		require.Equal(t, player0Name, player0.Name)

		game, err := getGameForPlayer(client, ts.URL, player0)
		require.NoError(t, err)
		require.Equal(t, player0.GameID, game.ID)

		// second player
		player1Name := "player1name"
		player1, err := joinGame(client, ts.URL, game.InviteCode, player1Name)
		require.NoError(t, err)
		require.Equal(t, player1.GameID, game.ID)

		// third player
		player2Name := "player2name"
		player2, err := joinGame(client, ts.URL, game.InviteCode, player2Name)
		require.NoError(t, err)
		require.Equal(t, player2.GameID, game.ID)

		// start game
		gameStarted, err := startGame(client, ts.URL, player1, game)
		require.NoError(t, err)
		require.Equal(t, model.GameStateRunning, gameStarted.Status)

		// fourth player, this needs to fail
		player3Name := "player3name"
		_, err = joinGame(client, ts.URL, game.InviteCode, player3Name)
		require.Error(t, err)

		dbGame, exists := storage.GetGameByID(game.ID)
		require.True(t, exists)
		require.Equal(t, 3, len(dbGame.PlayerIDs))
	})

	/*
		t.Run("some POST query with urlencode", func(t *testing.T) {
			params := url.Values{}
			params.Set("key", "value")
			req, err := http.NewRequest(http.MethodPost, ts.URL+"/endpointName", strings.NewReader(params.Encode()))
			require.NoError(t, err)
			req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
			req.Header.Add("Content-Length", strconv.Itoa(len(params.Encode())))

			resp, err := client.Do(req)
			require.NoError(t, err)

			require.Equal(t, 200, resp.StatusCode)
		})

		t.Run("PUT object", func(t *testing.T) {

			someObject := &some.Object{
				ID:             "0",
				Type:           "type",
			}
			bodyBytes, err := json.Marshal(someObject)
			require.NoError(t, err)

			req, err := http.NewRequest(http.MethodPut, ts.URL+"/endpointName/someMorePath", bytes.NewReader(bodyBytes))
			require.NoError(t, err)

			req.Header.Add("Launchpad-Auth", "sessionToken")

			resp, err := client.Do(req)
			require.NoError(t, err)
			require.Equal(t, 200, resp.StatusCode)
		})
	*/
}
