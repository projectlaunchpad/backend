/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package service_test

import (
	"crypto/tls"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/service"
	"gitlab.com/projectlaunchpad/backend/storage"
)

func TestStatusEndpoint(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	app, err := service.NewApplication(config)
	require.NoError(t, err)
	defer func() {
		os.Remove(storage.DatabaseFile)
		require.NoError(t, err)
	}()

	ts := httptest.NewServer(app.Engine)
	defer ts.Close()

	cookieJar, _ := cookiejar.New(nil)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr,
		Jar: cookieJar,
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}}

	t.Run("get status", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodGet, ts.URL+"/status", nil)
		require.NoError(t, err)

		resp, err := client.Do(req)
		require.NoError(t, err)

		require.Equal(t, 200, resp.StatusCode)
	})
}
