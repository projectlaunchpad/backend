/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package service_test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/projectlaunchpad/backend/model"
)

func createGame(client *http.Client, baseURL string, playerName string) (model.Player, error) {
	req, err := http.NewRequest(http.MethodGet, baseURL+"/game/create/"+playerName, nil)
	if err != nil {
		return model.Player{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return model.Player{}, err
	}
	if resp.StatusCode != 200 {
		return model.Player{}, fmt.Errorf("returned status code was %d", resp.StatusCode)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.Player{}, err
	}
	var player model.Player
	err = json.Unmarshal(bodyBytes, &player)
	if err != nil {
		return model.Player{}, err
	}
	return player, nil
}

func joinGame(client *http.Client, baseURL string, inviteCode string, playerName string) (model.Player, error) {
	req, err := http.NewRequest(http.MethodGet, baseURL+"/game/join/"+inviteCode+"/"+playerName, nil)
	if err != nil {
		return model.Player{}, err
	}
	resp, err := client.Do(req)
	if err != nil {
		return model.Player{}, err
	}
	if resp.StatusCode != 200 {
		return model.Player{}, fmt.Errorf("returned status code was %d", resp.StatusCode)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.Player{}, err
	}
	var player model.Player
	err = json.Unmarshal(bodyBytes, &player)
	if err != nil {
		return model.Player{}, err
	}
	return player, nil
}

func getGameForPlayer(client *http.Client, baseURL string, player model.Player) (model.Game, error) {
	req, err := http.NewRequest(http.MethodGet, baseURL+"/game/state/"+player.GameID, nil)
	if err != nil {
		return model.Game{}, err
	}
	req.Header.Add("Launchpad-Player", player.ID)
	req.Header.Add("Launchpad-Token", player.SessionToken)
	resp, err := client.Do(req)
	if err != nil {
		return model.Game{}, err
	}
	if resp.StatusCode != 200 {
		return model.Game{}, fmt.Errorf("returned status code was %d", resp.StatusCode)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.Game{}, err
	}
	var game model.Game
	err = json.Unmarshal(bodyBytes, &game)
	if err != nil {
		return model.Game{}, err
	}
	return game, nil
}

func startGame(client *http.Client, baseURL string, player model.Player, game model.Game) (model.Game, error) {
	req, err := http.NewRequest(http.MethodGet, baseURL+"/game/start/"+game.ID, nil)
	if err != nil {
		return model.Game{}, err
	}
	req.Header.Add("Launchpad-Player", player.ID)
	req.Header.Add("Launchpad-Token", player.SessionToken)
	resp, err := client.Do(req)
	if err != nil {
		return model.Game{}, err
	}
	if resp.StatusCode != 200 {
		return model.Game{}, fmt.Errorf("returned status code was %d", resp.StatusCode)
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return model.Game{}, err
	}
	var response model.Game
	err = json.Unmarshal(bodyBytes, &response)
	if err != nil {
		return model.Game{}, err
	}
	return response, nil
}
