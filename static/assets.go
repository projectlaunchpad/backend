//go:build dev
// +build dev

/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package static

import "net/http"

// Assets contains project assets.
var Assets http.FileSystem = http.Dir("assets")
