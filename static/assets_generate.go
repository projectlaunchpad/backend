//go:build ignore
// +build ignore

/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package main

import (
	"log"

	"github.com/shurcooL/vfsgen"

	"gitlab.com/projectlaunchpad/backend/static"
)

func main() {
	err := vfsgen.Generate(static.Assets, vfsgen.Options{
		PackageName:  "static",
		BuildTags:    "!dev",
		VariableName: "Assets",
		Filename:     "static/generated_assets.go",
	})
	if err != nil {
		log.Fatal(err)
	}
}
