/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

// Package endpoints provides database features.
package storage

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/spf13/viper"

	"gitlab.com/projectlaunchpad/backend/model"
	"gitlab.com/projectlaunchpad/backend/static"

	_ "github.com/mattn/go-sqlite3"
)

var instance *Storage

const DatabaseFile string = "./launchpad.sqlite"

const CardTypeResource string = "resource"
const CardTypeProject string = "project"
const CardTypeEvent string = "event"
const CardTypePersona string = "persona"

// Storage describes a database entity.
type Storage struct {
	config *viper.Viper
	db     *sql.DB

	ResourceCardIDs []string
	ProjectCardIDs  []string
	EventCardIDs    []string
	PersonaCardIDs  []string

	gameStore    map[string]model.Game
	playerStore  map[string]model.Player
	handStore    map[string]model.Hand
	projectStore map[string]model.Project
	stackStore   map[string]model.Stack
	actionStore  map[string]model.Action
}

func (s *Storage) GetCardByID(id string) (model.Card, bool) {
	allCards, error := s.GetAllCards()
	if error != nil {
		return model.Card{}, false
	}
	for _, c := range allCards {
		if c.ID == id {
			return c, true
		}
	}
	return model.Card{}, false
}

func (s *Storage) GetGameByID(id string) (model.Game, bool) {
	obj, exists := s.gameStore[id]
	if !exists {
		log.Printf("game %s not found in storage", id)
	}
	return obj, exists
}

func (s *Storage) GetGameByInviteCode(code string) (model.Game, bool) {
	for _, v := range s.gameStore {
		if v.InviteCode == code {
			return v, true
		}
	}
	return model.Game{}, false
}

func (s *Storage) PutCard(card model.Card) error {
	sqlStmt := `INSERT INTO cards
		(ID, type, name, description,illustrationid, iconid, effectid, effectargs, colors, cost, payout)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	// start db ta
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(sqlStmt)
	if err != nil {
		return err
	}
	defer stmt.Close()
	nameSerialized, err := json.Marshal(card.Name)
	if err != nil {
		return err
	}
	desriptionSerialized, err := json.Marshal(card.Description)
	if err != nil {
		return err
	}
	effectArgsSerialized, err := json.Marshal(card.EffectArgs)
	if err != nil {
		return err
	}
	colorsSerialized, err := json.Marshal(card.Colors)
	if err != nil {
		return err
	}
	payoutSerialized, err := json.Marshal(card.Payout)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(card.ID, card.Type, nameSerialized, desriptionSerialized, card.IllustrationID, card.IconID, card.EffectID, effectArgsSerialized, colorsSerialized, card.Cost, payoutSerialized)
	if err != nil {
		return err
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return updateCardTypeLists(s.db)
}

func (s *Storage) PutGame(obj model.Game) error {
	s.gameStore[obj.ID] = obj
	return nil
}

func (s *Storage) GetPlayerByID(id string) (model.Player, bool) {
	obj, exists := s.playerStore[id]
	return obj, exists
}

func (s *Storage) PutPlayer(obj model.Player) error {
	s.playerStore[obj.ID] = obj
	return nil
}

func (s *Storage) GetHandByID(id string) (model.Hand, bool) {
	obj, exists := s.handStore[id]
	if exists {
		obj.Size = len(obj.CardIDs)
	}
	return obj, exists
}

func (s *Storage) PutHand(obj model.Hand) error {
	obj.Size = len(obj.CardIDs)
	s.handStore[obj.ID] = obj
	return nil
}

func (s *Storage) GetProjectByID(id string) (model.Project, bool) {
	obj, exists := s.projectStore[id]
	return obj, exists
}

func (s *Storage) PutProject(obj model.Project) error {
	s.projectStore[obj.ID] = obj
	return nil
}

func (s *Storage) GetStackByID(id string) (model.Stack, bool) {
	obj, exists := s.stackStore[id]
	if exists {
		obj.Size = len(obj.CardIDs)
	}
	return obj, exists
}

func (s *Storage) PutStack(obj model.Stack) error {
	obj.Size = len(obj.CardIDs)
	s.stackStore[obj.ID] = obj
	return nil
}

func (s *Storage) GetActionByID(id string) (model.Action, bool) {
	obj, exists := s.actionStore[id]
	return obj, exists
}

func (s *Storage) PutAction(obj model.Action) error {
	s.actionStore[obj.ID] = obj
	return nil
}

func (s *Storage) RemoveProject(obj model.Project) error {
	delete(s.projectStore, obj.ID)
	return nil
}

func (s *Storage) GetAllCards() ([]model.Card, error) {
	queryStmt := "SELECT * FROM cards"
	rows, err := s.db.Query(queryStmt)
	if err != nil {
		return []model.Card{}, err
	}
	allCards := []model.Card{}
	for rows.Next() {
		card, err := readCardFromRows(rows)
		if err != nil {
			return []model.Card{}, err
		}
		allCards = append(allCards, card)
	}
	return allCards, nil
}

func readCardFromRows(rows *sql.Rows) (model.Card, error) {
	card := model.Card{}
	var nameBuf, descriptionBuf, effectArgsBuf, colorsBuf, payoutBuf string
	err := rows.Scan(&card.ID, &card.Type, &nameBuf, &descriptionBuf, &card.IllustrationID, &card.IconID, &card.EffectID, &effectArgsBuf, &colorsBuf, &card.Cost, &payoutBuf)
	if err != nil {
		return model.Card{}, err
	}
	err = json.Unmarshal([]byte(nameBuf), &card.Name)
	if err != nil {
		return model.Card{}, err
	}
	err = json.Unmarshal([]byte(descriptionBuf), &card.Description)
	if err != nil {
		return model.Card{}, err
	}
	err = json.Unmarshal([]byte(effectArgsBuf), &card.EffectArgs)
	if err != nil {
		return model.Card{}, err
	}
	err = json.Unmarshal([]byte(colorsBuf), &card.Colors)
	if err != nil {
		return model.Card{}, err
	}
	err = json.Unmarshal([]byte(payoutBuf), &card.Payout)
	if err != nil {
		return model.Card{}, err
	}
	return card, nil
}

func isDatabaseEmpty(db *sql.DB) (bool, error) {
	rows, err := db.Query("SELECT name FROM sqlite_master WHERE type='table' AND name='games'")
	if err != nil {
		return false, err
	}
	return !rows.Next(), nil
}

func bootstrapDatabase(db *sql.DB) error {
	_, err := db.Exec(model.SQLCreateCards)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreateGames)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreatePlayers)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreateHands)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreateProjects)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreateStacks)
	if err != nil {
		return err
	}
	_, err = db.Exec(model.SQLCreateActions)
	if err != nil {
		return err
	}
	return nil
}

// NewMock creates a new mock database instance.
func NewStorage(config *viper.Viper) (*Storage, error) {
	log.Println("preparing database store..")
	db, err := sql.Open("sqlite3", DatabaseFile)
	if err != nil {
		return nil, err
	}
	dbEmpty, err := isDatabaseEmpty(db)
	if err != nil {
		return nil, err
	}
	if dbEmpty {
		bootstrapDatabase(db)
	}
	loadCardsIntoDB(db)
	instance = &Storage{
		config:          config,
		db:              db,
		ResourceCardIDs: make([]string, 0),
		ProjectCardIDs:  make([]string, 0),
		EventCardIDs:    make([]string, 0),
		PersonaCardIDs:  make([]string, 0),
		gameStore:       make(map[string]model.Game),
		playerStore:     make(map[string]model.Player),
		handStore:       make(map[string]model.Hand),
		projectStore:    make(map[string]model.Project),
		stackStore:      make(map[string]model.Stack),
		actionStore:     make(map[string]model.Action),
	}
	err = updateCardTypeLists(db)
	if err != nil {
		return nil, err
	}
	log.Println("database store ready")
	return instance, nil
}

// RemoveDatabase closes the database.
func (s *Storage) Close() error {
	err := s.db.Close()
	if err != nil {
		return err
	}
	instance = nil
	return nil
}

// GetStorage returns the storage instance.
func GetStorage() *Storage {
	return instance
}

func updateCardTypeLists(db *sql.DB) error {
	allCards, err := GetStorage().GetAllCards()
	if err != nil {
		return err
	}
	for _, card := range allCards {
		switch card.Type {
		case CardTypeResource:
			instance.ResourceCardIDs = append(instance.ResourceCardIDs, card.ID)
		case CardTypeProject:
			instance.ProjectCardIDs = append(instance.ProjectCardIDs, card.ID)
		case CardTypeEvent:
			instance.EventCardIDs = append(instance.EventCardIDs, card.ID)
		case CardTypePersona:
			instance.PersonaCardIDs = append(instance.PersonaCardIDs, card.ID)
		default:
			return fmt.Errorf("encountered unknown card type %s", card.Type)
		}
	}
	return nil
}

// loadCardsIntoDB populates the storage with data from a json file.
func loadCardsIntoDB(db *sql.DB) error {
	// read card database
	cardsFile, err := static.Assets.Open("/cards.json")
	if err != nil {
		return err
	}
	defer func() {
		if err = cardsFile.Close(); err != nil {
			log.Panic(err)
		}
	}()
	cardData, err := ioutil.ReadAll(cardsFile)
	if err != nil {
		return err
	}
	allCards := []model.Card{}
	err = json.Unmarshal(cardData, &allCards)
	if err != nil {
		return err
	}
	// prepare statement
	sqlStmt := `INSERT INTO cards
		(ID, type, name, description,illustrationid, iconid, effectid, effectargs, colors, cost, payout)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`
	// start db ta
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	stmt, err := tx.Prepare(sqlStmt)
	if err != nil {
		return err
	}
	defer stmt.Close()
	// purge database table cards
	db.Exec("DELETE FROM cards")
	for _, card := range allCards {
		nameSerialized, err := json.Marshal(card.Name)
		if err != nil {
			return err
		}
		desriptionSerialized, err := json.Marshal(card.Description)
		if err != nil {
			return err
		}
		effectArgsSerialized, err := json.Marshal(card.EffectArgs)
		if err != nil {
			return err
		}
		colorsSerialized, err := json.Marshal(card.Colors)
		if err != nil {
			return err
		}
		payoutSerialized, err := json.Marshal(card.Payout)
		if err != nil {
			return err
		}
		_, err = stmt.Exec(card.ID, card.Type, nameSerialized, desriptionSerialized, card.IllustrationID, card.IconID, card.EffectID, effectArgsSerialized, colorsSerialized, card.Cost, payoutSerialized)
		if err != nil {
			return err
		}
	}
	tx.Commit()
	return nil
}
