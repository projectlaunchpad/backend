/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package storage_test

import (
	"os"
	"testing"

	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"

	"gitlab.com/projectlaunchpad/backend/storage"
)

func TestStorage(t *testing.T) {

	config := viper.New()
	config.SetDefault("httpurl", ":8888")
	config.SetDefault("externalhttpurl", "http://localhost:8888")
	config.AutomaticEnv()

	// starting from a clean database
	os.Remove(storage.DatabaseFile)
	storageInst, err := storage.NewStorage(config)
	require.NoError(t, err)
	require.NotNil(t, storageInst)
	defer func() {
		storageInst.Close()
		os.Remove(storage.DatabaseFile)
		require.NoError(t, err)
	}()

	t.Run("init card storage", func(t *testing.T) {
		//require.Equal(t, 2, len(storage.resourceCards))
	})
}
