/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils

// Contains checks if an entity is contained in an entity slice.
func Contains(s []interface{}, str interface{}) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

// ContainsString checks if a string is contained in a string slice.
func ContainsString(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}
	return false
}

// ContainsAll checks if all elements of query exists in target.
func ContainsAll(target []interface{}, query []interface{}) bool {
	if len(query) == 0 {
		return false
	}
	for _, v := range query {
		if !Contains(target, v) {
			return false
		}
	}
	return true
}

// ContainsAllString checks if all elements of query exists in target.
func ContainsAllString(target []string, query []string) bool {
	if len(query) == 0 {
		return false
	}
	for _, v := range query {
		if !ContainsString(target, v) {
			return false
		}
	}
	return true
}

// IndexOf returns the index of an element in a slice or -1 if not found.
func IndexOf(element string, data []string) int {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1
}
