/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/projectlaunchpad/backend/utils"
)

func TestAuthenticationEndpoint(t *testing.T) {

	t.Run("ContainsString", func(t *testing.T) {
		s := []string{"a0", "a1"}
		str := "a1"
		require.True(t, utils.ContainsString(s, str))
	})

	// FIXME: add more tests
}
