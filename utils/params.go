/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"reflect"
	"strconv"
)

// GetJSONQueryParam retrieves a param from a gin context.
func GetJSONQueryParam(ctx *gin.Context, key string) (interface{}, bool, error) {
	paramStr, paramAvailable := ctx.GetQuery(key)
	if !paramAvailable {
		return nil, false, nil
	}
	var j interface{}
	err := json.Unmarshal([]byte(paramStr), &j)
	return j, true, err
}

// HasParamFilterID checks if there is a param filter={ID:[id1, id2]} in the request.
func HasParamFilterID(ctx *gin.Context) (bool, error) {
	queryParam, available, err := GetJSONQueryParam(ctx, "filter")
	if err != nil {
		return false, fmt.Errorf("error getting query parameter 'filter' from request: %s", err.Error())
	}
	if !available {
		return false, nil
	}
	queryMap, ok := queryParam.(map[string]interface{})
	if !ok {
		return false, fmt.Errorf("query parameter 'filter' is not a map")
	}
	if val, ok := queryMap["ID"]; ok {
		if !ok {
			return false, fmt.Errorf("query parameter 'filter.id' is not in the map")
		}
		sliceVal, ok := val.([]interface{})
		if !ok {
			return false, fmt.Errorf("query parameter 'filter.id' is not a slice")
		}
		for _, entry := range sliceVal {
			if reflect.ValueOf(entry).Kind() != reflect.String {
				return false, fmt.Errorf("found entry in parameter 'filter.id' that is not a string")
			}
		}
		return true, nil
	}
	return false, nil
}

// GetJSONParamFilterID retrieves a parameter { ID: [ 1, 2, 3]} as an string array.
func GetJSONParamFilterID(ctx *gin.Context) ([]string, bool, error) {
	key := "filter"
	queryParam, available, err := GetJSONQueryParam(ctx, key)
	if err != nil {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: %s", key, err.Error())
	}
	if !available {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: parameter not found", key)
	}
	queryMap, ok := queryParam.(map[string]interface{})
	if !ok {
		return nil, available, fmt.Errorf("query parameter '%s' is not a map", key)
	}
	if val, ok := queryMap["ID"]; ok {
		if !ok {
			return nil, available, fmt.Errorf("query parameter 'filter.id' is not in the map")
		}
		sliceVal, ok := val.([]interface{})
		if !ok {
			return nil, available, fmt.Errorf("query parameter 'filter.id' is not a slice")
		}
		result := make([]string, 0)
		for _, entry := range sliceVal {
			if reflect.ValueOf(entry).Kind() != reflect.String {
				return nil, available, fmt.Errorf("found entry in parameter 'filter.ID' that is not a string")
			}
			result = append(result, entry.(string))
		}
		return result, available, nil
	}
	return nil, available, fmt.Errorf("query parameter '%s.ID' is not in request", key)
}

// GetJSONQueryParamsMap retrieves a parameter as a map.
func GetJSONQueryParamsMap(ctx *gin.Context, key string) (map[string]interface{}, bool, error) {
	queryParam, available, err := GetJSONQueryParam(ctx, key)
	if err != nil {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: %s", key, err.Error())
	}
	if !available {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: parameter not found", key)
	}
	queryMap, ok := queryParam.(map[string]interface{})
	if !ok {
		return nil, available, fmt.Errorf("query parameter '%s' is not a map", key)
	}
	return queryMap, available, nil
}

// GetJSONQueryParamsStringArray retrieves a parameter as a string array.
func GetJSONQueryParamsStringArray(ctx *gin.Context, key string) ([]string, bool, error) {
	queryParam, available, err := GetJSONQueryParam(ctx, key)
	if err != nil {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: %s", key, err.Error())
	}
	if !available {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: parameter not found", key)
	}
	queryArray, ok := queryParam.([]interface{})
	if !ok {
		return nil, available, fmt.Errorf("query parameter '%s' is not an array", key)
	}
	resultArray := make([]string, 0)
	for _, entry := range queryArray {
		var valueStr string
		entryValue := reflect.ValueOf(entry)
		switch entryValue.Kind() {
		case reflect.Float64:
			valueStr = strconv.FormatFloat(entry.(float64), 'f', 2, 64)
		case reflect.String:
			valueStr = entry.(string)
		case reflect.Bool:
			valueStr = strconv.FormatBool(entry.(bool))
		default:
			log.Printf("query parameter 'filter.%s' is not of a supported type %s", key, entryValue.Kind().String())
			return nil, available, fmt.Errorf("entry '%s' in query parameter '%s' is not of a supported type: '%s'", entry, key, entryValue.Kind().String())
		}
		resultArray = append(resultArray, valueStr)
	}
	return resultArray, available, nil
}

// GetJSONQueryParamsIntArray retrieves a parameter as a int array.
func GetJSONQueryParamsIntArray(ctx *gin.Context, key string) ([]int, bool, error) {
	queryParam, available, err := GetJSONQueryParam(ctx, key)
	if err != nil {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: %s", key, err.Error())
	}
	if !available {
		return nil, available, fmt.Errorf("error getting query parameter '%s' from request: parameter not found", key)
	}
	queryArray, ok := queryParam.([]interface{})
	if !ok {
		return nil, available, fmt.Errorf("query parameter '%s' is not an array", key)
	}
	resultArray := make([]int, 0)
	for _, entry := range queryArray {
		var valueStr int
		switch t := entry.(type) {
		case float64:
			valueStr = int(entry.(float64))
		default:
			log.Printf("query parameter 'filter.%s' is not of a supported type %s", key, t)
			return nil, available, fmt.Errorf("entry '%s' in query parameter '%s' is not of a supported type: '%s'", entry, key, t)
		}
		resultArray = append(resultArray, valueStr)
	}
	return resultArray, available, nil
}
