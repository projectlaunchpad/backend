/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */
package utils

// SystemStatus returns the system status.
func SystemStatus() string {
	return "nominal - green"
}
