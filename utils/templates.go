/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils

import (
	"strings"

	"github.com/shurcooL/httpfs/vfsutil"
	"gitlab.com/projectlaunchpad/backend/static"
)

// GetHTMLTemplate returns the html with the replacements applied.
func GetHTMLTemplate(filename string, replaceMap map[string]string) (string, error) {
	html, err := vfsutil.ReadFile(static.Assets, filename)
	if err != nil {
		return "", err
	}
	s := string(html)
	for key, value := range replaceMap {
		s = strings.ReplaceAll(s, "{{"+key+"}}", value)
	}
	return s, nil
}
