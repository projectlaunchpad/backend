/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils

import (
	"strings"

	"github.com/rs/xid"
)

// CreateUUID creates a new uuid.
func CreateUUID() string {
	return xid.New().String()
}

// CreateInviteCode creates a short verification code.
func CreateInviteCode() string {
	uuid := strings.ToLower(CreateUUID())
	return uuid[len(uuid)-8:]
}
