/*
 * Launchpad Backend
 * Copyright 2020 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

package utils

// Version describes the version of the build.
var Version = "0.0.1"

// Build describes the git commit of the build.
var Build = "n/a"

// BuildDate describes the build date.
var BuildDate = "n/a"
